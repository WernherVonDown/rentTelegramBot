import dotenv from 'dotenv';

dotenv.config();

export const vars = {
    apiUrl: process.env.API_URL || 'http://127.0.0.1:4000/api',
    checkPeriod: Number(process.env.CHECK_PERIOD || 30000),
    telegram: {
        token: process.env.TELEGRAM_TOKEN || '',
        chatId: process.env.TELEGRAM_CHAT_ID || '',
    },
    envTag: process.env.ENV_TAG || 'unknown',
}