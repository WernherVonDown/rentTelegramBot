import { sendHttpRequest } from "../../utils/http/sendHttpRequest";


const MAX_MESSAGE_LENGTH = 4096;

interface IArgs {
    chatId: string;
    botToken: string;
    message: string;
}

export const sendMessageToChannel = async ({ chatId, botToken, message }: IArgs) => {
    if (!chatId || !botToken) {
        console.log("error; sendMessageToChannel: chatId and botToken are required")
        return;
    }
    await sendHttpRequest({
        url: `https://api.telegram.org/bot${botToken}/sendMessage?chat_id=${chatId}&text=${encodeURIComponent(message)}`,
        method: 'get',
        data: {},
    });
};
