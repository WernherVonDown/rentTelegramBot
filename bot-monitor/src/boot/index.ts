import { healthchecker } from "../utils/healthchecker"

export const startBoot = async () => {
    healthchecker()
    console.log('boot:complete')
}
