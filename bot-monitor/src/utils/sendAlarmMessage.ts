import { vars } from "../config/vars"
import { sendMessageToChannel } from "../externalServices/telegram/sendMessageToChannel"

export const sendAlarmMessage = async (message: string) => {
    try {
        await sendMessageToChannel({chatId: vars.telegram.chatId, botToken: vars.telegram.token, message})
    } catch (error) {
        console.log("sendAlarmMessage error", error);
    }
}
