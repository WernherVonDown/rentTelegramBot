import axios, { Method, CancelToken, AxiosRequestConfig } from 'axios';

interface IArgs extends AxiosRequestConfig {
    method: Method;
    url: string;
    data: any;
    params?: Record<string, any>;
    headers?: Record<string, any>;
    cancelToken?: CancelToken;
    onUploadProgress?: (progressEvent: any) => void;
    onDownloadProgress?: (progressEvent: any) => void;
    options?: Record<string, unknown>;
}

export const sendHttpRequest = (args: IArgs) => {
    const {
        url,
        method,
        data,
        params,
        headers,
        cancelToken,
        onUploadProgress,
        onDownloadProgress,
        ...options
    } = args;
    return axios({
        url,
        method,
        data,
        params,
        headers,
        cancelToken,
        onUploadProgress,
        onDownloadProgress,
        ...options,
    });
};
