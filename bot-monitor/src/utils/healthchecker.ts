import path from "path";
import { vars } from "../config/vars";
import { sendHttpRequest } from "./http/sendHttpRequest";
import { sendAlarmMessage } from "./sendAlarmMessage";

let wasError: boolean | undefined;
let firstStart = true;

interface IError {
    text: string;
    count: number;
}

const sendPingRequest = async () => {
    sendHttpRequest({
        url: vars.apiUrl + '/ping',
        method: 'get',
        data: {},
    }).then(e => {
        if (e.data.errors.length) {
            const errorMsg = e.data.errors.map((e: IError) => `\n${e.text} (${e.count})`).join('');
            sendAlarmMessage(`[error] service ${vars.envTag}${errorMsg}`)
            wasError = true;
        } else if (firstStart || wasError) {
            sendAlarmMessage(`Service ${vars.envTag} is ok`);
            wasError = false;
        }
    }).catch(e => {
        if (firstStart || !wasError) {
            sendAlarmMessage(`Service ${vars.envTag} not responding. Cannot ping ${vars.apiUrl}`);
            wasError = true;
        }
        
    }).finally(() => {
        firstStart = false;
    });
}

export const healthchecker = () => {
    console.log("healthchecker start")
    
    setInterval(sendPingRequest, vars.checkPeriod)
}
