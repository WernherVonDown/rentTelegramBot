import { startBoot } from "./boot"

const start = async () => {
    await startBoot()
}

start().catch(e => console.log("start error", e))