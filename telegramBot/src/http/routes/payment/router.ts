import { Router } from "express";
import * as controller from './controller';

const router = Router();



router.get('/:subscriptionPlanId', controller.getPaymentLink);

router.post('/status', controller.paymentStatus);

export default router;
