import { PaymentStatuses } from "@a2seven/yoo-checkout";
import { NextFunction, Request, Response } from "express";
import { billService } from "../../../services/bill.service";
import { paymentService } from "../../../services/payment.service";
import { subscriptionService } from "../../../services/subscription.service";
import { userService } from "../../../services/user.service";
import { yookassaService } from "../../../services/yookassa.service";

export const getPaymentLink = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { subscriptionPlanId } = req.params;
        const { userId } = req.query
        console.log("BILL ID", subscriptionPlanId, userId)
        if (!subscriptionPlanId || !userId) {
            throw new Error("No required parameters")
        }
        
        const activeSubscription = await subscriptionService.isUserSubscriptionActiveById(userId as string, subscriptionPlanId);

        if (activeSubscription) {
            await userService.sendTelegramMessage(userId as string, `Вы уже подписаны до ${activeSubscription.expiresAt}`)
            console.log(`Error; try to pay paid subscription userId=${userId}; subscription=${JSON.stringify(activeSubscription)}`)
            throw new Error(`Try to pay paid subscription`)
        }

        const billUrl = await paymentService.getBillUrl(userId as string, subscriptionPlanId);
        console.log("BILL URL", billUrl)
        return res.redirect(billUrl);
    } catch (error: any) {
        console.log("getPaymentLink; error", error)
        return res.send("error: " + error.message)
    }
}

export const paymentStatus = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { object: { id: pId, status, payment_method: { id, card, title }, metadata: { billId, userId, userName } } } = req.body
        console.log("SUCCESS", req.body)
        if (status === PaymentStatuses.succeeded) {
            const bill = await billService.payBill(billId, pId, '');
        const sub = await subscriptionService.createUserSubscription(bill.user, bill.subscriptionPlan);
        await userService.sendTelegramMessage(bill.user, `Вы успешно подписаны на 24 часа`)
        await userService.sendTelegramMessageToAdmins(`Пользователь ${userName} оплатил подписку`)
        } else if (status === PaymentStatuses.waiting_for_capture) {
            await yookassaService.capture(pId).then(e => console.log("CAPTURED", e)).catch(e => console.log("ERRORR CAPTURE", pId, e))
        }
        return res.sendStatus(200);
    } catch (error) {
        console.log("paymentSuccess; error", error)
        return res.sendStatus(200);
    }
}