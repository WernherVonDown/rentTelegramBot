import paymentRoutes from "./payment/router";

import { Router } from "express";
import errorsStore from "../../ustils/errorsStore";


const router = Router();

router.use('/payment', paymentRoutes);

router.get('/hello', (req, res) => res.send('Hello world!'))
router.get('/ping', (req, res) => {
    res.send({errors: errorsStore.getErrors()})
    errorsStore.clearErrors();
});

export default router;