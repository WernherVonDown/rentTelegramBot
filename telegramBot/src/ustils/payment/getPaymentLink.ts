import { vars } from "../../config/vars";
import { yookassaService } from "../../services/yookassa.service";
interface params {
    orderId: string | number,  
    serviceName?: string, 
    price: number,
    currency: string,
    email?: string,
    name: string,
}

const DEFAULT_EMAIL = vars.intellectMoney.customerEmail;
const SHOP_ID = vars.intellectMoney.shopId;
const INN = vars.intellectMoney.inn;
const CUSTOMER_MAIL = vars.intellectMoney.customerEmail;

export const getPaymentLink = ({orderId, serviceName = 'telegram bot', price, currency, email = DEFAULT_EMAIL, name}: params) => 
encodeURI(`https://merchant.intellectmoney.ru/ru?eshopId=${SHOP_ID}&orderId=${orderId}&serviceName=${serviceName}&recipientAmount=${price}&recipientCurrency=${currency}&user_email=${email}&preference=bankcard&merchantReceipt={"inn":"${INN}","group":"Main","content":{"type":1,"positions":[{"quantity":1.000,"price":"${price}","tax":1,"text":"${name}"}],"customerContact":"${CUSTOMER_MAIL}"}}`)

export const getPaymentYooKassLink = async ({billId, userId, userName, price, description = ''}: {billId: string, userId: string, userName: string, price: string | number, description?: string}) => {
    const payment = await yookassaService.createPayment({billId, userId, userName, price, description})
    return payment.confirmation.confirmation_url || '';
}

