import url from 'url';
import { vars } from '../../config/vars';

export const getBillLink = (subscriptionId: string, userId: string | number) => vars.apiUrl + `/payment/${subscriptionId}?userId=${userId}`;
