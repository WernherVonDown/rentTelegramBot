interface IError {
    text: string;
    count: number;
}

let errors: IError[] = [];

const addErrorMsg = (msg: string) => {
    const isCoptyIndex = errors.findIndex(e => e.text === msg);
    if (~isCoptyIndex) {
        errors[isCoptyIndex].count++;
    } else {
        errors.push({text: msg, count: 1});
    }
}

const getErrors = () => errors;

const clearErrors = () => {
    errors = []
}

export default {
    addErrorMsg,
    getErrors,
    clearErrors,
}