import { IInlineOption } from '../../const/types';
export const makeOption = (text: string, textCB?: any): IInlineOption => ({ text, cb: textCB || text });
export const makeOptionUrl = (text: string, url: string): IInlineOption => ({ text, url });
