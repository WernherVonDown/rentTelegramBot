import { Markup } from "telegraf";
import { IInlineOption } from '../../const/types';

export enum ButtonTypes {
    inline = 'inline',
    keyboard = 'keyboard',
}

export enum MessageTypes {
    text = 'text',
    photo = 'photo',
    media_group = 'media_group',
}

export const getMessageWithDocument = (
    document: string,
    text: string,
    buttons?: IInlineOption[] | any,
    type: ButtonTypes = ButtonTypes.inline
) => {
    const res: any = getMessage(text, buttons, type);
    res.type = MessageTypes.media_group;
    res.document = document;
    return res;
}

export const getPhoneMessage = (
    text: string,
    buttons: IInlineOption[] | any,
) => {
    const reply_markup: any = {}
    if (buttons) {
        reply_markup.keyboard = [buttons.map((b: string) => ({ text: b, request_contact: true }))]
        reply_markup.resize_keyboard = true;
        reply_markup.one_time_keyboard = true;
    }
    return { message: text, reply_markup, type: MessageTypes.text, }
}

export const getMessageWithPhoto = (
    photoId: string,
    text: string,
    buttons?: IInlineOption[] | any,
    type: ButtonTypes = ButtonTypes.inline
) => {
    const res: any = getMessage(text, buttons, type);
    res.type = MessageTypes.photo;
    res.photo = photoId;
    return res;
}

export const getMessage = (
    text: string,
    buttons?: IInlineOption[] | any,
    type: ButtonTypes = ButtonTypes.inline
) => {
    const reply_markup: any = {}

    if (buttons) {
        if (type === ButtonTypes.inline) {
            reply_markup.inline_keyboard = buttons.map((b: IInlineOption) => ([{ text: b.text, callback_data: b.cb, url: b.url }]))
        } else if (type === ButtonTypes.keyboard) {
            reply_markup.keyboard = buttons.map((b: string) => [({ text: b, callback_data: 'Hello mister loh' })])
            reply_markup.resize_keyboard = true;
        }
        // inlineRes = Markup.inlineKeyboard(
        //     inline_buttons
        //     .map(o => [Markup.button.callback(o.text, o.cb)]))
    }

    // console.log('KEYBOARD', keyboardButtons)

    // if (keyboardButtons) {
    //     keyboardRes = Markup
    //     .keyboard(
    //         keyboardButtons
    //         .map(o => o)).oneTime().resize();
    // }

    // console.log('KEYBOARD2', keyboardRes)


    return { message: text, reply_markup, type: MessageTypes.text }//,parse_mode: 'Markdown'
    // }};
}

// export const getMessage = (
//     text: string,
//     inline_buttons?: IInlineOption[],
//     keyboardButtons?: string[]
// ) => {
//     let inlineRes = {}
//     let keyboardRes = {}

//     if (inline_buttons) {
//         inlineRes = Markup.inlineKeyboard(
//             inline_buttons
//             .map(o => [Markup.button.callback(o.text, o.cb)]))
//     }

//     console.log('KEYBOARD', keyboardButtons)

//     if (keyboardButtons) {
//         keyboardRes = Markup
//         .keyboard(
//             keyboardButtons
//             .map(o => o)).oneTime().resize();
//     }

//     console.log('KEYBOARD2', keyboardRes)


//     return [text, {...inlineRes, ...keyboardRes
//         , parse_mode: 'Markdown'
//     }];
// }