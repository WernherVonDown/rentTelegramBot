export = {
    seconds: function (secs: number) {return secs * 1000},
    minutes: function (mins: number) {return this.seconds(mins) * 60},
    hours: function (hours: number) {return this.minutes(hours) * 60},
    days: function (days: number) {return this.hours(days) * 24},
}