export function cleanEmptyObjectFields(obj: any = {}, value?: string | number) {
    for (var propName in obj) {
        if (obj[propName] === null || obj[propName] === undefined || obj[propName] === value) {
            delete obj[propName];
        }
    }
    return obj
}