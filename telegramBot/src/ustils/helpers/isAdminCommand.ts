export const adminCommands = [
    '/matches',
    '/rmUser',
    '/auth',
    '/users',
    '/orders',
    '/subs',
]

export const isAdminCommand = (text: string) => adminCommands.includes(parseCommand(text).command);

export const parseCommand = (text: string) => ({command: text.split(' ')[0], param: text.split(' ').slice(1).join(' ')})