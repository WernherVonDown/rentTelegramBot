const last_message_store: {[key: number]: number} = {};

export const addLastMessage = (chatId: number, messageId: number) => {
    last_message_store[chatId] = messageId;
}

export const getLastMessage = (chatId: number) => last_message_store[chatId];
