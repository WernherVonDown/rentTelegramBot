import dotenv from 'dotenv';
import mongoose from 'mongoose';

dotenv.config();

export default async function () {
  await mongoose.connect(process.env.MONGO_URL || 'mongodb://localhost:27060/rentBot');
  console.log('Connected to DB')
}
