import { runMigrations } from '../migrations';
import { startAddressBot } from './startAddressBot';
import startMongo from './startMongo';
import { startServer } from './startServer';

export const startBoot = async () => {
    startAddressBot()
    await startServer()
    await startMongo()
    runMigrations()
}
