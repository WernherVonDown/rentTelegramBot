
import { app } from "../config/express";
import { vars } from "../config/vars";

export const startServer = async () => {
    app.listen(vars.serverPort, () => console.log(`server is running on ${vars.serverPort}`));
};
