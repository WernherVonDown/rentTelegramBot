import dotenv from 'dotenv';
import { Telegraf } from 'telegraf';
import { REGISTER_STAGES } from '../const/scenario/REGISTER_STAGES';
import authInlineMiddleware from '../middlewares/authInline.middleware';
import { addressService } from '../services/address.service';
import { flatService } from '../services/flat.service';

dotenv.config();

const token = process.env.TELEGRAM_ADDRESS_BOT_API_TOKEN || '';

const bot = new Telegraf(token);



export const startAddressBot = () => {
  bot.use(authInlineMiddleware);
  bot.on('inline_query', async (ctx) => {
    try {
      console.log("INLINE", ctx.inlineQuery.query)
      const user = ctx.state.user;
      if (!user) {
        const res = {
          type: 'article', title: 'Ошибка доступа', 
          id: Date.now(), 
          input_message_content: {
            message_text: 'Ошибка доступа'
          },
          description: 'Пользователь не найден'
        }
        //@ts-ignore
        return ctx.answerInlineQuery([res]);
      }

      if (user.stage !== REGISTER_STAGES.address) {
        const res = {
          type: 'article', title: 'Ошибка доступа', 
          id: Date.now(), 
          input_message_content: {
            message_text: 'Ошибка доступа'
          },
          description: 'Бот доступен только во время регистрации адреса.'
        }
        //@ts-ignore
        return ctx.answerInlineQuery([res]);
      }

      const flat = await flatService.getDocument(user.flat || 'no');

      if (!flat) {
        const res = {
          type: 'article', title: 'Ошибка', 
          id: Date.now(), 
          input_message_content: {
            message_text: 'Ошибка'
          },
          description: 'Жилье не найдено'
        }
        //@ts-ignore
        return ctx.answerInlineQuery([res]);
      }

      const query = `${flat.city} ${ctx.inlineQuery.query}`;
      console.log('QUERY', query)
      const res = await addressService.completeAddress(query)
      // console.log("RES2", res[0] && res[0].data)
      const results = res.map((r: any, i: number) => (
        {
          type: 'article', title: r?.data?.city_with_type || r?.data?.region, id: (i + '' + Date.now()), input_message_content: {
            message_text: r.value
          },
          description: r.value
        }
      ))
      //@ts-ignore
      await ctx.answerInlineQuery(results)
    } catch (error) {
      console.log("INLINE  QUERY REsult error", error)
    }

  })

  bot.on('chosen_inline_result', ({ chosenInlineResult }) => {
    console.log('chosen inline result', chosenInlineResult)
  })

  bot.launch();
}