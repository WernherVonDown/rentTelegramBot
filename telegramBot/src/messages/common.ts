import { vars } from "../config/vars";

export default {
    start: 'Хей! Я помогу тебе решить квартирный вопрос. Чтобы начать поиск, заполни анкету, это займет меньше минуты!',
    more_variants: 'Еще варианты',
    settings: 'Настройки',
    not_matters: 'Неважно',
    choose_options: 'Выберите из предложенных вариантов',
    help: `В случае вопросов, проблем, предложений обращайтесь к ${vars.helpContact}`,
}