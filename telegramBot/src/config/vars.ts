import dotenv from 'dotenv';

dotenv.config();

export const vars = {
    maxMatches: 5,
    serverPort: process.env.SERVER_PORT || 4000,
    apiUrl: process.env.API_URL || 'http://127.0.0.1:4000/api',
    intellectMoney: {
        defaultEmail: process.env.IMONEY_CUSTOMER_MAIL,
        shopId: process.env.IMONEY_SHOP_ID,
        inn: process.env.IMONEY_INN,
        customerEmail: process.env.IMONEY_CUSTOMER_MAIL,
    },
    adminSecret: process.env.ADMIN_SECRET || '123456',
    helpContact: process.env.HELP_CONTACT || '@RentRoom_Help',
    yookassa: {
        secretKey: process.env.UKASSA_API_KEY || '',
        shopId: process.env.UKASSA_SHOP_ID || '',
    },
    telegram: {
        token: process.env.TELEGRAM_TOKEN || '',
        chatId: process.env.TELEGRAM_CHAT_ID || '',
    },
    envTag: process.env.ENV_TAG || 'unknown',
}