import express from "express";
import cors from "cors";
// import { vars } from "../config/vars";
import routes from "../http/routes/";
import bodyParser from "body-parser";

const app = express();

// const isLocal = vars.env === "develop";

const corsOptions = {
    // origin: isLocal ? "http://localhost:3000" : undefined,
    optionsSuccessStatus: 200,
};

app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/api", routes);

export { app };
