import { optionsSettingsController } from "../../controllers/settings.controller";
import { IUserDocument } from "../../models/User.model";
import { getMessage } from "../../ustils/telegram/getMessage";
import { makeOption } from "../../ustils/telegram/makeOption";

export const SETTINGS_PREFIX = 'settings';

export const SettingsActions =  {
    HIDE: `${SETTINGS_PREFIX}_hide`,
    REREGISTER: `${SETTINGS_PREFIX}_reregister`,
    SUSPEND_MATCH_LIMIT: `${SETTINGS_PREFIX}_suspendMatchLimit`,
}

export const common_settings = {
    message: 'Настройки',
        options: [
            makeOption('Снять лимит откликов', SettingsActions.SUSPEND_MATCH_LIMIT),
            makeOption('Скрыть объявление', SettingsActions.HIDE),
            makeOption('Перепройти регистрацию', SettingsActions.REREGISTER),
        ],
        controller: function (user: IUserDocument, text: string): Promise<any[]> {
            return optionsSettingsController(this.options, user,  text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
}