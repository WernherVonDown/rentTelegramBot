import { optionsRegisterController, endRegister, startRegister, addingPhoto, endAddingPhoto, textRegisterController } from '../../controllers/register.controller';
import { IUserDocument } from '../../models/User.model';
import { ButtonTypes, getMessage, getPhoneMessage } from '../../ustils/telegram/getMessage';
import { register_renter_scenario } from './register_renter';
import { makeOption } from '../../ustils/telegram/makeOption';
import { Roles } from '../../const/user/ROLES';
import { register_rentee_scenario } from './register_rentee';
import { endRegisterStage, REGISTER_STAGES, startRegisterStage } from '../../const/scenario/REGISTER_STAGES';
import common from '../../messages/common';

export const RENTEE_REGISTER_STAGES_ORDER = [
    REGISTER_STAGES.city,
    REGISTER_STAGES.gender,
    REGISTER_STAGES.livers,
    REGISTER_STAGES.pets,
    REGISTER_STAGES.smoking,
    REGISTER_STAGES.nation,
    REGISTER_STAGES.work,
    REGISTER_STAGES.metro_destance,
    REGISTER_STAGES.number_of_rooms,
    REGISTER_STAGES.price_from,
    REGISTER_STAGES.price_to,
    REGISTER_STAGES.period,
    REGISTER_STAGES.address,
    REGISTER_STAGES.description,
    REGISTER_STAGES.photo,
    REGISTER_STAGES.phone_number,
]
//REGISTER_STAGES.renter_info_reg_part, // NO
export const RENTER_REGISTER_STAGES_ORDER = [
    REGISTER_STAGES.city,
    REGISTER_STAGES.metro_destance,
    REGISTER_STAGES.number_of_rooms,
    REGISTER_STAGES.price_from,
    REGISTER_STAGES.price_to,
    REGISTER_STAGES.period,
    REGISTER_STAGES.gender,
    REGISTER_STAGES.livers,
    REGISTER_STAGES.pets,
    REGISTER_STAGES.smoking,
    REGISTER_STAGES.nation,
    REGISTER_STAGES.work,
    REGISTER_STAGES.photo,
    REGISTER_STAGES.phone_number,
    REGISTER_STAGES.description,
]

export const common_register = {
    [startRegisterStage]: {
        message: 'Вы хотите:',
        options: [
            makeOption('Найти жильё', Roles.renter),
            makeOption('Сдать жильё', Roles.rentee),
        ],
        dbField: 'role',
        controller: async function (user: IUserDocument, text: string) {
            console.log('controller', startRegisterStage, user)
            return startRegister(this.options, this.dbField, user, text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    [endRegisterStage]: {
        message: 'Спасибо, за регистрацию! Мы постараемся вам помочь как можно скорее!',
        dbField: '',
        options: [
            makeOption('Посмотреть варианты'),
        ],
        controller: function (user: IUserDocument, text: string) {
            return endRegister(this.options, this.dbField, user,  text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    [REGISTER_STAGES.city]: {
        message: 'Выберите город:',
        options: [
            makeOption('Москва'),
            makeOption('Санкт-Петербург'),
            makeOption('Екатеринбург'),
            makeOption('Иркутск'),
        ],
        dbField: 'city',
        controller: function (user: IUserDocument, text: string) {
            return optionsRegisterController(this.options, this.dbField, user, text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    [REGISTER_STAGES.number_of_rooms]: {
        message: 'Вид жилья:',
        options: [
            makeOption('Комната'),
            makeOption('Студия'),
            makeOption('1-комнатная'),
            makeOption('2 и более комнат'),
        ],
        dbField: 'number_of_rooms',
        controller: function (user: IUserDocument, text: string) {
            return optionsRegisterController(this.options, this.dbField, user,  text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    [REGISTER_STAGES.metro_destance]: {
        message: 'Удалённость от метро/остановки пешком:',
        options: [
            makeOption('Менее 5 минут'),
            makeOption('От 5 до 15 минут'),
            makeOption('От 15 до 25 минут'),
            makeOption('Более 25 минут'),
            makeOption(common.not_matters)
        ],
        dbField: 'metro_distance',
        controller: function (user: IUserDocument, text: string) {
            return optionsRegisterController(this.options, this.dbField, user, text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    [REGISTER_STAGES.gender]: {
        message: 'Пол:',
        options: [
            makeOption('м'),
            makeOption('ж'),
            makeOption(common.not_matters),
        ],
        dbField: 'gender',
        controller: function (user: IUserDocument, text: string) {
            return optionsRegisterController(this.options, this.dbField, user,  text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    [REGISTER_STAGES.pets]: {
        message: 'Животные:',
        options: [
            makeOption('Без животных'),
            makeOption('небольшие'),
            makeOption(common.not_matters),
        ],
        dbField: 'pets',
        controller: function (user: IUserDocument, text: string) {
            return optionsRegisterController(this.options, this.dbField, user,  text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    [REGISTER_STAGES.smoking]: {
        message: 'Курение в квартире:',
        options: [
            makeOption('Разрешено'),
            makeOption('Запрещено'),
            makeOption(common.not_matters),
        ],
        dbField: 'smoking',
        controller: function (user: IUserDocument, text: string) {
            return optionsRegisterController(this.options, this.dbField, user,  text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    [REGISTER_STAGES.nation]: {
        message: 'Национальность:',
        options: [
            makeOption('гражданство РФ'),
            makeOption('славянин'),
            makeOption(common.not_matters),
        ],
        dbField: 'nation',
        controller: function (user: IUserDocument, text: string) {
            return optionsRegisterController(this.options, this.dbField, user,  text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    [REGISTER_STAGES.livers]: {
        message: 'Количество проживающих:',
        options: [
            makeOption('Один человек'),
            makeOption('Пара'),
            makeOption('Пара с детьми'),
            makeOption('Компания'),
            makeOption(common.not_matters),
        ],
        dbField: 'livers',
        controller: function (user: IUserDocument, text: string) {
            return optionsRegisterController(this.options, this.dbField, user,  text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    [REGISTER_STAGES.work]: {
        message: 'Трудоустройство',
        options: [
            makeOption('Работающий'),
            makeOption('Студент'),
            makeOption(common.not_matters),
        ],
        dbField: 'work',
        controller: function (user: IUserDocument, text: string) {
            return optionsRegisterController(this.options, this.dbField, user,  text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    [REGISTER_STAGES.period]: {
        message: 'Предполагаемый срок аренды:',
        options: [
            makeOption('Посуточно'),
            makeOption('Несколько месяцев'),
            makeOption('Долгосрочная'),
            makeOption(common.not_matters),
        ],
        dbField: 'period',
        controller: function (user: IUserDocument, text: string) {
            return optionsRegisterController(this.options, this.dbField, user, text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    [REGISTER_STAGES.photo]: {
        message: `Добавьте несколько фото, чтобы вас выбрали охотнее!\nПервое добавленное фото будет иллюстрировать ваше предложение`,
        keyboard: ['Пропустить'],
        controller: function(user: IUserDocument, photoId: string) {
            return addingPhoto(user, photoId);
        },
        endController: function(user: IUserDocument) {
            return endAddingPhoto(user);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.keyboard, ButtonTypes.keyboard);
        }
    },
    [REGISTER_STAGES.price_from]: {
        message: `Укажите минимальную цену в рублях.\nНапример: 17000, 25300`,
        dbField: 'price_from',
        controller: function (user: IUserDocument, price: string) {
            const num = +price;
            console.log("PRICE", num, price)
            if (isNaN(num)) {
                return [this.prepareMessage('Введите число')]
            } else if (num < 0) {
                return [this.prepareMessage("Отрицательное число? Хах, введи нормальную цену )")]
            } else if (num > 150000) {
                return [this.prepareMessage("Ничего себе цена... Попробуйте ввести меньшую цену (<150000) и люди к вам потянутся :)")]
            }
            return textRegisterController(num, this.dbField, user);
        },
        prepareMessage: function (message?: string) {
            return getMessage(message || this.message, undefined)
        }
    },
    [REGISTER_STAGES.price_to]: {
        message: `Укажите максимальную цену в рублях.\nПримеры: 26000, 55700`,
        dbField: 'price_to',
        controller: async function (user: IUserDocument, price: string) {
            const num = +price;
            const order = await user.getOrder()
            if (isNaN(num)) {
                return [this.prepareMessage()]
            } else if (num < 0) {
                return [this.prepareMessage("Отрицательное число? Хах, введи нормальную цену )")]
            } else if (num < order.price_from) {
                return [this.prepareMessage("Введите цену больше или равную минимальной")]
            }
            return textRegisterController(price, this.dbField, user);;
        },
        prepareMessage: function (message?: string) {
            return getMessage(message || this.message, undefined)
        }
    },
    [REGISTER_STAGES.description]: {
        message: `Дополнительная информация/пожелания. Текстом в одном сообщении.`,
        // keyboard: ['Поделиться номером'],
        dbField: 'description',
        controller: function(user: IUserDocument, text: string) {
            return textRegisterController(text, this.dbField, user);
        },
        prepareMessage: function () {
            return getMessage(this.message, undefined);
        }
    },
}

export const register_renter: {
    [key: string]: any,
} = {
    ...common_register,
    ...register_renter_scenario,
}
export const register_rentee:{
    [key: string]: any,
   } = {
    ...common_register,
    ...register_rentee_scenario,
}