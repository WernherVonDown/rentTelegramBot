// import { REGISTER_STAGES } from '.';

import { endRegisterStage, REGISTER_STAGES } from "../../const/scenario/REGISTER_STAGES";
import { addingPhoto, endAddingPhoto, endRegister, optionsRegisterController, textRegisterController } from "../../controllers/register.controller";
import common from "../../messages/common";
import { IUserDocument } from "../../models/User.model";
import { ButtonTypes, getMessage, getPhoneMessage } from "../../ustils/telegram/getMessage";
import { makeOption } from "../../ustils/telegram/makeOption";

export const register_renter_scenario: {
    [key: string]: any,
} = {
    [REGISTER_STAGES.renter_info_reg_part]: {
        message: `Ваши параметры сохранены, при желании, их можно будет изменить позднее. 
        Теперь ответьте на несколько вопросов о себе, это поможет арендодателю найти вас как можно скорее.`,
        noAnswer: true,
        controller: function (user: IUserDocument, phone: string) {
            return {}
        },
        prepareMessage: function () {
            return getMessage(this.message, undefined)
        }
    },
    [REGISTER_STAGES.gender]: {
        message: 'Укажите Ваш пол:',
        options: [
            makeOption('Мужской'),
            makeOption('Женский'),
        ],
        dbField: 'gender',
        controller: function (user: IUserDocument, text: string) {
            return optionsRegisterController(this.options, this.dbField, user,  text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    [REGISTER_STAGES.pets]: {
        message: 'Наличие домашних животных:',
        options: [
            makeOption('Есть крупные'),
            makeOption('Небольшие'),
            makeOption('Нет'),
        ],
        dbField: 'pets',
        controller: function (user: IUserDocument, text: string) {
            return optionsRegisterController(this.options, this.dbField, user, text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    // [REGISTER_STAGES.smoking]: {
    //     message: 'Курение',
    //     options: [
    //         makeOption('В квартире'),
    //         makeOption('На балконе'),
    //         makeOption('Не курю'),
    //     ],
    //     dbField: 'smoking',
    //     controller: function (user: IUserDocument, text: string) {
    //         return optionsRegisterController(this.options, this.dbField, user, text);
    //     },
    //     prepareMessage: function () {
    //         return getMessage(this.message, this.options);
    //     }
    // },
    [REGISTER_STAGES.nation]: {
        message: 'Ваша национальная принадлежность, гражданство',
        options: [
            makeOption('Гражданство РФ'),
            makeOption('Гражданство стран СНГ'),
            makeOption('Иностранное'),
        ],
        dbField: 'nation',
        controller: function (user: IUserDocument, text: string) {
            return optionsRegisterController(this.options, this.dbField, user, text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    [REGISTER_STAGES.work]: {
        message: 'Ваш род занятий',
        options: [
            makeOption('Студент'),
            makeOption('Есть работа'),
        ],
        dbField: 'work',
        controller: function (user: IUserDocument, text: string) {
            return optionsRegisterController(this.options, this.dbField, user, text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    [REGISTER_STAGES.photo]: {
        message: `Добавьте Вашу фотографию, чтобы Вас охотнее выбрали. 
        Первая загруженная фотография будет являться обложкой Вашей анкеты.
        Также можно загружать фотографии приживающих вместе с Вами или животных.
        После завершения нажмите «Готово»`,
        keyboard: ['Пропустить'],
        controller: function (user: IUserDocument, photoId: string, isPhotoController?: boolean) {
            return addingPhoto(user, photoId, isPhotoController);
        },
        endController: function (user: IUserDocument) {
            return endAddingPhoto(user);
        },
        prepareMessage: function (msg?: string) {
            return getMessage(msg || this.message, this.keyboard, ButtonTypes.keyboard);
        }
    },
    [REGISTER_STAGES.phone_number]: {
        message: `Поделитесь Вашим номером телефона или контактом в «Телеграмм». 
        Обмен контактами происходит только после согласия обеих сторон для дальнейшего общения с арендодателем.`,
        keyboard: ['Поделиться номером'],
        dbField: 'phone',
        controller: function (user: IUserDocument, phone: string) {
            return textRegisterController(phone, this.dbField, user);;
        },
        prepareMessage: function () {
            return getPhoneMessage(this.message, this.keyboard)//getMessage(this.message);
        }
    },
    [endRegisterStage]: {
        message: 'Спасибо за регистрацию! Ваше идеальное жильё уже ждет Вас!\nНажните на кнопку «Посмотреть варианты», сначала будут показаны наиболее подходящие Вам варианты.',
        dbField: '',
        options: [
            makeOption('Посмотреть варианты'),
        ],
        controller: function (user: IUserDocument, text: string) {
            return endRegister(this.options, this.dbField, user,  text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
}
