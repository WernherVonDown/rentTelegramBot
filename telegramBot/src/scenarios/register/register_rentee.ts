import { endRegisterStage, REGISTER_STAGES } from "../../const/scenario/REGISTER_STAGES";
import { addressRegisterController, endRegister } from "../../controllers/register.controller";
import { addingPhoto, endAddingPhoto, optionsRegisterController, textRegisterController } from "../../controllers/register.controller";
import common from "../../messages/common";
import { IUserDocument } from "../../models/User.model";
import { ButtonTypes, getMessage, getMessageWithDocument, getMessageWithPhoto, getPhoneMessage } from "../../ustils/telegram/getMessage";
import { makeOption } from "../../ustils/telegram/makeOption";
export const register_rentee_scenario: {
    [key: string]: any,
   } = {
    [REGISTER_STAGES.address]: {
//         message: 
// `Укажите адрес дома.Чтобы воспользоваться ботом напиште: r_adresbot и начните адрес дома.\nПример: r_adresbot Ленина 25 к 3`,
        message: "Укажите адрес дома\nВажно! Для удобства заполнения перед вводом адреса напишите: @R_ADRESBOT\nНапример:\n@R_ADRESBOT Проспект Большевиков д 11 корпус 2\n@R_ADRESBOT Ленина дом 25 к 3",
        // keyboard: ['Поделиться номером'],
        dbField: 'location',
        controller: function(user: IUserDocument, address: string) {
            return addressRegisterController(address, this.dbField, user);
        },
        prepareMessage: function () {
            // const msg = getMessage(this.message, undefined)//getMessage(this.message);
            //@ts-ignore
            const msg = getMessageWithDocument('https://storage.yandexcloud.net/mystorage1444/IMG_6246.gif', this.message, undefined);
            //@ts-ignore
            msg.parse_mode = "HTML"
            return msg;
        }
    },
    [REGISTER_STAGES.gender]: {
        message: 'Укажите пол арендатора:',
        options: [
            makeOption('Мужской'),
            makeOption('Женский'),
            makeOption(common.not_matters),
        ],
        dbField: 'gender',
        controller: function (user: IUserDocument, text: string) {
            return optionsRegisterController(this.options, this.dbField, user, text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    [REGISTER_STAGES.pets]: {
        message: 'Допускаете ли вы проживание домашних животных в квартире:',
        options: [
            makeOption('Есть крупные'),
            makeOption('Небольшие'),
            makeOption('Нет'),
            makeOption(common.not_matters),
        ],
        dbField: 'pets',
        controller: function (user: IUserDocument, text: string) {
            return optionsRegisterController(this.options, this.dbField, user, text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    // [REGISTER_STAGES.smoking]: {
    //     message: 'Разрешено ли курить в вашей квартире',
    //     options: [
    //         makeOption('Разрешено'),
    //         makeOption('Разрешено на балконе'),
    //         makeOption('Запрещено'),
    //         makeOption(common.not_matters),
    //     ],
    //     dbField: 'smoking',
    //     controller: function (user: IUserDocument, text: string) {
    //         return optionsRegisterController(this.options, this.dbField, user, text);
    //     },
    //     prepareMessage: function () {
    //         return getMessage(this.message, this.options);
    //     }
    // },
    [REGISTER_STAGES.nation]: {
        message: 'Национальная принадлежность, гражданство проживающего',
        options: [
            makeOption('Гражданство РФ'),
            makeOption('Гражданство стран СНГ'),
            makeOption('Иностранное'),
            makeOption(common.not_matters),
        ],
        dbField: 'nation',
        controller: function (user: IUserDocument, text: string) {
            return optionsRegisterController(this.options, this.dbField, user, text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    [REGISTER_STAGES.livers]: {
        message: 'Укажите количество проживающих:',
        options: [
            makeOption('Один человек'),
            makeOption('Пара'),
            makeOption('Пара с детьми'),
            makeOption('Компания'),
            makeOption(common.not_matters),
        ],
        dbField: 'livers',
        controller: function (user: IUserDocument, text: string) {
            return optionsRegisterController(this.options, this.dbField, user, text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    [REGISTER_STAGES.work]: {
        message: 'Род занятий квартиранта',
        options: [
            makeOption('Студент'),
            makeOption('Есть работа'),
            makeOption(common.not_matters),
        ],
        dbField: 'work',
        controller: function (user: IUserDocument, text: string) {
            return optionsRegisterController(this.options, this.dbField, user, text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
    [REGISTER_STAGES.photo]: {
        message: `Добавьте фотографию вашей квартиры, чтобы Вас охотнее выбрали. 
        Первая загруженная фотография будет являться обложкой Вашей анкеты.
        После завершения нажмите «Готово»`,
        keyboard: ['Пропустить'],
        controller: function (user: IUserDocument, photoId: string, isPhotoController?: boolean) {
            return addingPhoto(user, photoId, isPhotoController);
        },
        endController: function (user: IUserDocument) {
            return endAddingPhoto(user);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.keyboard, ButtonTypes.keyboard);
        }
    },
    [REGISTER_STAGES.renter_info_reg_part]: {
        message: `Ваши параметры сохранены, при желании, их можно будет изменить позднее.\nТеперь ответьте на несколько вопросов вашей квартире, это поможет арендодателю найти вас как можно скорее.`,
        noAnswer: true,
        controller: function (user: IUserDocument, phone: string) {
            return {}
        },
        prepareMessage: function () {
            return getMessage(this.message, undefined)
        }
    },
    [REGISTER_STAGES.description]: {
        message: `Напишите дополнительную информацию касаемо жильца или квартиры текстом в одном сообщении`,
        // keyboard: ['Поделиться номером'],
        dbField: 'description',
        controller: function (user: IUserDocument, text: string) {
            return textRegisterController(text, this.dbField, user);
        },
        prepareMessage: function () {
            return getMessage(this.message, undefined);
        }
    },
    [REGISTER_STAGES.phone_number]: {
        message: `Поделитесь Вашим номером телефона или контактом в «Телеграмм». 
        Обмен контактами происходит только после согласия обеих сторон для дальнейшего общения с арендатором.`,
        keyboard: ['Поделиться номером'],
        dbField: 'phone',
        controller: function (user: IUserDocument, phone: string) {
            return textRegisterController(phone, this.dbField, user);;
        },
        prepareMessage: function () {
            return getPhoneMessage(this.message, this.keyboard)//getMessage(this.message);
        }
    },
    [endRegisterStage]: {
        message: 'Спасибо за регистрацию! Ваш идеальный квартирант уже ждет Вас!\nНажните на кнопку «Посмотреть варианты», сначала будут показаны наиболее подходящие Вам варианты.',
        dbField: '',
        options: [
            makeOption('Посмотреть варианты'),
        ],
        controller: function (user: IUserDocument, text: string) {
            return endRegister(this.options, this.dbField, user,  text);
        },
        prepareMessage: function () {
            return getMessage(this.message, this.options);
        }
    },
}