import { startOfferController, offerController, chooseOfferController, matchOfferController } from '../../controllers/offer.controller';
import { IUserDocument } from "../../models/User.model";
import { getMessage, getMessageWithPhoto } from "../../ustils/telegram/getMessage";
import { makeOption } from "../../ustils/telegram/makeOption";
import { IFlatDocument } from '../../models/Flat.model';
import { IOrderDocument } from '../../models/Order.model';

export const offerStage = 'offer';

export const offer_scenario = {
    [offerStage]: {
        message: 'Что вы хотите сделать?',
        getOptions: function (id: string, numberOfPhotos: number, geo_lat: string, geo_lon: string) {
            if (geo_lat && geo_lon) {
                return [
                    makeOption(`Посмотреть все фото (${numberOfPhotos})`, `photos_${id}`),
                    makeOption('Посмотреть на карте', `location_${geo_lat}_${geo_lon}`),
                    makeOption('Выбрать', `choose_${id}`),
                ] 
            }
            return [
                makeOption(`Посмотреть все фото (${numberOfPhotos})`, `photos_${id}`),
                makeOption('Выбрать', `choose_${id}`),
            ]
        },
        getChooseOptions: function (id: string, numberOfPhotos: number, geo_lat: string, geo_lon: string) {
            if (geo_lat && geo_lon) {
                return [
                    makeOption(`Посмотреть все фото (${numberOfPhotos})`, `photos_${id}`),
                    makeOption('Посмотреть на карте', `location_${geo_lat}_${geo_lon}`),
                    makeOption('Обменяться контактами', `match_${id}`),
                ] 
            }
            
            return [
                makeOption(`Посмотреть все фото (${numberOfPhotos})`, `photos_${id}`),
                makeOption('Обменяться контактами', `match_${id}`),
            ]
        },
        getMatchOptions: function (id: string, numberOfPhotos: number, geo_lat: string, geo_lon: string) {
            if (geo_lat && geo_lon) {
                return [
                    makeOption('Посмотреть на карте', `location_${geo_lat}_${geo_lon}`),
                    makeOption(`Посмотреть все фото (${numberOfPhotos})`, `photos_${id}`),
                ]
            }
            
            return [
                makeOption(`Посмотреть все фото (${numberOfPhotos})`, `photos_${id}`),
            ]
        },
        // options: [
        //     makeOption('Выбрать'),
        // ],
        keyboard: [
            'Показать еще',
        ],
        controller: async function (user: IUserDocument): Promise<any> {
            console.log('controller offer')
            return offerController(user);
        },
        chooseController: async function
            (
                chooserDocument: IFlatDocument | IOrderDocument,
                chooseeDocument: IFlatDocument | IOrderDocument,
            ): Promise<any> {
                return chooseOfferController(chooserDocument, chooseeDocument)
        },
        matchController: async function
            (
                matcherDocument: IFlatDocument | IOrderDocument,
                matcheeDocument: IFlatDocument | IOrderDocument,
            ): Promise<any> {
                return matchOfferController(matcherDocument, matcheeDocument)
        },
        prepareChooseMessage: function (chooserVariant: string, id: string, photoId?: string | undefined, numberOfPhotos?: number, geo_lat: string = '', geo_lon: string = '') {
            if (!photoId) {
                return getMessage(chooserVariant, this.getChooseOptions(id, numberOfPhotos || 0, geo_lat, geo_lon))
            }

            return getMessageWithPhoto(photoId, chooserVariant, this.getChooseOptions(id, numberOfPhotos || 0, geo_lat, geo_lon));
        },
        prepareMatchMessage: function (chooserVariant: string, id: string, photoId?: string | undefined, numberOfPhotos?: number, geo_lat: string = '', geo_lon: string = '') {
            if (!photoId) {
                return getMessage(chooserVariant, this.getMatchOptions(id, numberOfPhotos || 0, geo_lat, geo_lon))
            }

            return getMessageWithPhoto(photoId, chooserVariant, this.getMatchOptions(id, numberOfPhotos || 0, geo_lat, geo_lon));
        },
        prepareMessage: function (offerVariant: string, id: string, photoId?: string | undefined, numberOfPhotos?: number, geo_lat: string = '', geo_lon: string = '') {
            if (!photoId) {
                return getMessage(offerVariant, this.getOptions(id, numberOfPhotos || 0, geo_lat, geo_lon))
            }
            return getMessageWithPhoto(photoId, offerVariant, this.getOptions(id, numberOfPhotos || 0,geo_lat, geo_lon));
        }
    }
}