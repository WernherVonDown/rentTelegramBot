import { Schema, model, Document } from 'mongoose';
import { Roles } from '../const/user/ROLES';
import { flatService } from '../services/flat.service';
import { orderService } from '../services/order.service';
import { IFlatDocument } from './Flat.model';
import { IOrderDocument } from './Order.model';

const User = new Schema({
    firstName: { type: String, unique: false, required: true },
    lastName: { type: String, unique: false, required: false },
    userName: { type: String, unique: false, required: false },
    chatId: { type: Number, unique: true, required: true },
    role: { type: String, unique: false, required: false },
    languageCode: { type: String, unique: false, required: true },
    stage: { type: String, unique: false, required: false },
    order: {
        type: Schema.Types.ObjectId,
        ref: 'Order',
        required: false
    },
    flat: {
        type: Schema.Types.ObjectId,
        ref: 'Flat',
        required: false
    },
    activeFillOrderId: { type: String, unique: false, required: false },
    seenOrders: [{
        type: Schema.Types.ObjectId,
        ref: 'Order',
        required: false
    }],
    seenFlats: [{
        type: Schema.Types.ObjectId,
        ref: 'Flat',
        required: false
    }],
    isAdmin: {
        type: Boolean,
        default: false,
    }
});

export interface IUserDocument extends Document {
    firstName: string,
    lastName: string,
    userName: string,
    chatId: string,
    languageCode: string,
    stage: string,
    activeFillOrderId: string,
    order: string,
    flat: string,
    role: string,
    seenOrders: string[],
    seenFlats: string[],
    getOrder: () => Promise<IOrderDocument | IFlatDocument>,
    isAdmin: boolean,
}

User.methods.getOrder = async function () {
    if (this.role === Roles.rentee) {
        return flatService.getDocumentWithOwner(this.flat);
    } else {
        return orderService.getDocumentWithOwner(this.order);
    }
}

export default model('User', User);