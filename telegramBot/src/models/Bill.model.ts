import { model, Schema } from 'mongoose';

const Bill = new Schema({
    user: {
        ref: 'User',
        type: Schema.Types.ObjectId,
    },
    subscriptionPlan: {
        ref: 'SubscriptionPlan',
        type: Schema.Types.ObjectId,
    },
    createdAt: {
        type: String,
    },
    paidAt: {
        type: String
    },
    paid: {
        type: Boolean,
        default: false,
    },
    userEmail: {
        type: String
    },
    paymentId: {
        type: String
    },
})

export default model('Bill', Bill);
