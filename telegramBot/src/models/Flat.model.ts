import {Schema, model, Document} from 'mongoose';
import { IUserDocument } from './User.model';

const Flat = new Schema({
    city: {type: String, unique: false, required: false},
    metro_distance: {type: String, unique: false, required: false},
    status: {type: String, unique: false, required: false},
    gender: {type: String, unique: false, required: false},
    pets: {type: String, unique: false, required: false},
    smoking: {type: String, unique: false, required: false},
    nation: {type: String, unique: false, required: false},
    livers: {type: String, unique: false, required: false},
    work: {type: String, unique: false, required: false},
    period: {type: String, unique: false, required: false},
    photos: [{type: String, unique: false, required: false}],
    phone: {type: String, unique: false, required: false},
    price_from: {type: Number, unique: false, required: false},
    price_to: {type: Number, unique: false, required: false},
    owner: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: false
    },
    location: {
        geo_lat: {type: String},
        geo_lon: {type: String},
        address: {type: String},
        metro: [
            {
              distance: {type: Number},
              line: {type: String},
              name: {type: String},
            }
        ]
    },
    description: { type: String, unique: false, required: false },
    isActive: { type: Boolean, default: true },
    number_of_rooms: {type: String},
    complete:  { type: Boolean, default: false },
});

export interface IFlatDocument extends Document {
    city: string,
    metro_distance: string,
    status: string,
    gender: string,
    pets: string,
    smoking: string,
    nation: string,
    livers: string,
    work: string,
    period: string,
    owner: string | IUserDocument,
    photos: string[];
    phone: string;
    price_from: number;
    price_to: number;
    location: ILocation;
    description: string;
    isActive: boolean;
    number_of_rooms: string;
    complete: boolean;
}

export interface ILocation {
    geo_lat: string,
    geo_lon: string,
    address: string,
    metro: IMetro[]
}

export interface IMetro {
    distance: number,
    line: string,
    name: string,
  }

export default model('Flat', Flat);
