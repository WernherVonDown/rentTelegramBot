import { Schema, model, Document } from 'mongoose';

const SubscriptionPlan = new Schema({
    price: {
        type: Number,
        required: true,
    },
    currency: {
        type: String,
        default: 'TST', //RUB
    },
    title: {
        type: String,
        equired: true,
    },
    description: {
        type: String,
    },
    benifits: [
        {
            type: String
        }
    ],
    discount: {
        type: Number,
    },
    discontEndDate: {
        type: String,
    },
    period: {
        type: Number,
    },
    key: {
        type: String,
    },
})

export interface ISubscriptionPlan extends Document {
    price: number;
    currency: string;
    period: number;
    title: string;
}

// SubscriptionPlan.methods.toDto = function () {
//     return {
//         id: this._id,
//         level: this.level,
//         price: this.price,
//         currency: this.currency,
//         title: this.title,
//         description: this.description,
//         benifits: this.benifits,
//         discount: this.discount,
//         discontEndDate: this.discontEndDate,
//         maxLangs: this.maxLangs,
//         bonusCoef: this.bonusCoef,
//     }
// }

export default model<ISubscriptionPlan>('SubscriptionPlan', SubscriptionPlan);
