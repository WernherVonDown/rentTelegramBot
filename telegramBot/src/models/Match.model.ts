import { Schema, model } from 'mongoose';

const Match = new Schema({
    completed: {
        type: Boolean,
    },
    initiator: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    initiatorOrder: {
        type: String,
    },
    targetOrder: {
        type: String,
    },
    target: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    startTime: {
        type: Number,
    },
    endTime: {
        type: Number,
    },
    counted: {
        type: Boolean,
    }
});

export default model('Match', Match);


