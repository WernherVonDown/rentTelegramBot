import { Schema, model } from 'mongoose';

const Migration = new Schema({
    type: {
        type: String,
        required: true,
    }
})

export default model('Migration', Migration);
