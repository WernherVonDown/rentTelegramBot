import { Schema, model } from "mongoose";

const Subscription = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    plan: {
        type: Schema.Types.ObjectId,
        ref: 'SubscriptionPlan',
    },
    createdAt: {
        type: String,
    },
    expiresAt: {
        type: String,
    },
    active: {
        type: Boolean,
        default: false,
    }
})

export default model('Subscription', Subscription);
