import { IUserDocument } from '../models/User.model';
import { offerStage, offer_scenario } from '../scenarios/offer';
import { userService } from '../services/user.service';
import { ButtonTypes, getMessage } from '../ustils/telegram/getMessage';
import { IFlatDocument, ILocation, IMetro } from '../models/Flat.model';
import { IOrderDocument } from '../models/Order.model';
import messages from '../messages';
import common from '../messages/common';
import { Roles } from '../const/user/ROLES';

export const getOfferMessage = (document: IFlatDocument | IOrderDocument) => {
    let metro_distance = document.metro_distance ? `\nрасстояние до метро: *${document.metro_distance}*` : "";
    const smoking = document.smoking ? `\nкурение: *${document.smoking}*` : "";
    const nation = document.nation ? `\nнациональность: *${document.nation}*` : "";
    const period = document.period ? `\nсрок: *${document.period}*` : "";
    const work = document.work ? `\nтрудоустройство: *${document.work}*` : "";
    const price_from = document.price_from ? `от ${document.price_from}р ` : "";
    const price_to = document.price_to ? `до ${document.price_to}р` : "";
    const price = `\nцена: *${(price_from || price_to) ? `${price_from}${price_to}` : "-"}*`
    const description = document.description ? `\nДополнительная информация: *${document.description}*`: ""
    let address = "";
    //@ts-ignore
    const loc = document?.location;
    if (loc as ILocation) {
        address = `\nАдрес: *${loc.address}*`
        const metro = loc.metro?.length ? loc.metro.map((e: IMetro) => `${e.name} ${e.distance}км`) : ['-']
        metro_distance = `\nБлижайшие станции метро: *${metro.join(`\n`)}*`
    }
    return `город: *${document.city}*${address}${metro_distance}${smoking}${nation}${period}${work}${price}${description}`;
}

export const getOfferMessages = (results: IFlatDocument[] | IOrderDocument[] = []) => (
    results.map(getOfferMessage)
)

export const startOfferController = async (user: IUserDocument) => {
    const results = await userService.getBestOptions(user, 1);
    const scenario = offer_scenario[offerStage];
    console.log("FOUND RESULTS 111", results.length)
    const messages = getOfferMessages(results);
    // console.log("FOUND RESULTS 111 1", results)
    const photo = (results.length && results[0].photos?.length && results[0].photos[0]) || undefined;
    // console.log("FOUND RESULTS 111 2", photo)
    const photosLength = (results.length && results[0].photos?.length) || 0;
    // console.log("FOUND RESULTS 111 3", photosLength)
    const { geo_lat, geo_lon} = getLocation(results[0] as IFlatDocument);
    return messages.length ?
        [
            getMessage('Подберите себе идеальный вариант', [common.more_variants, common.settings], ButtonTypes.keyboard),
            scenario.prepareMessage(messages[0], results[0]._id, photo, photosLength, geo_lat, geo_lon)
        ] : [getMessage(`Не нашли подходящий вариантов, попробуйте позже`)]

}

export const getContacts = (document: IFlatDocument | IOrderDocument) => {
    const phone = document.phone ? `телефон: ${document.phone}` : 'Телефон не предоставлен';
    const user = document.owner as IUserDocument;

    const userName = user?.userName ? `Пользователь @${user?.userName}` : 'Пользователь неизвестно';
    const name = `Имя пользователя: ${user?.firstName || 'не предоставлено'} ${user.lastName || ''}`
    return `${phone}\n${userName}\n${name}`;
}

export const matchOfferController = async (
    matcherDocument: IFlatDocument | IOrderDocument,
    matcheeDocument: IFlatDocument | IOrderDocument
) => {
    const scenario = offer_scenario[offerStage];
    const photo = matcherDocument?.photos?.length && matcherDocument.photos[0] || undefined;
    const photosLength = matcherDocument?.photos?.length || 0;
    const offerMessage = getOfferMessage(matcherDocument);
    const { geo_lat, geo_lon} = getLocation(matcherDocument as IFlatDocument);
    const message = `Пользователь согласился обемняться контактами:\n${getContacts(matcherDocument)}\n====\n${offerMessage}`
    return [scenario.prepareMatchMessage(message, matcherDocument._id, photo, photosLength, geo_lat, geo_lon)]
}

export const notifyAdminNewOrder = async (user: IUserDocument) => {
    user = await userService.getUserById(user._id)
    const scenario = offer_scenario[offerStage];
    const userDocument = await user.getOrder();
    console.log("NOTIFY ADMIN", user, userDocument)
    const photo = userDocument?.photos?.length && userDocument.photos[0] || undefined;
    const photosLength = userDocument?.photos?.length || 0;
    const offerMessage = getOfferMessage(userDocument);
    const { geo_lat, geo_lon} = getLocation(userDocument as IFlatDocument);
    const message = `Новый ${user.role === Roles.rentee ? 'арендодатель' : 'арендатор'}:\n${getContacts(userDocument)}\n====\n${offerMessage}`;
    const resultMessage = scenario.prepareMatchMessage(message, userDocument._id, photo, photosLength, geo_lat, geo_lon);
    await userService.sendTelegramMessageToAdmins('', resultMessage)
}

export const chooseOfferController = async (
    chooserDocument: IFlatDocument | IOrderDocument,
    chooseeDocument: IFlatDocument | IOrderDocument
) => {
    const scenario = offer_scenario[offerStage];
    const photo = chooserDocument?.photos?.length && chooserDocument.photos[0] || undefined;
    const photosLength = chooserDocument?.photos?.length || 0;
    const offerMessage = getOfferMessage(chooserDocument);
    const message = `Пользователь предлагает обменяться с вами контакатми\n=======\n${offerMessage}`
    const { geo_lat, geo_lon} = getLocation(chooserDocument as IFlatDocument);
    return [scenario.prepareChooseMessage(message, chooserDocument._id, photo, photosLength, geo_lat, geo_lon)]
}

export const offerController = async (user: IUserDocument) => {
    const results = await userService.getBestOptions(user, 1);
    const scenario = offer_scenario[offerStage];
    const messages = getOfferMessages(results);
    const photo = results.length && results[0].photos.length && results[0].photos[0] || undefined;
    const photosLength = (results.length && results[0].photos.length) || 0;
    const { geo_lat, geo_lon} = getLocation(results[0] as IFlatDocument);
    return messages.length ? [scenario.prepareMessage(messages[0], results[0]._id, photo, photosLength, geo_lat, geo_lon)] : [getMessage(`Кажется, вы всё посмотрели :( Мы вам напишем, когда появятся новые подходящие предложения.`)]
}

const getLocation = (document: IFlatDocument) => {
    const loc = document?.location;
    if (loc) {
        return { geo_lat: loc.geo_lat, geo_lon: loc.geo_lon };
    }

    return { geo_lat: '', geo_lon: '' }
}