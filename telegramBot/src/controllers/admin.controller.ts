import { Context } from "telegraf";
import { vars } from "../config/vars";
import { IFlatDocument } from "../models/Flat.model";
import { IOrderDocument } from "../models/Order.model";
import { IUserDocument } from "../models/User.model";
import { flatService } from "../services/flat.service";
import { matchService } from "../services/match.service";
import { orderService } from "../services/order.service";
import { subscriptionService } from "../services/subscription.service";
import { userService } from "../services/user.service";
import { adminCommands, parseCommand } from "../ustils/helpers/isAdminCommand";

export const onAdminCommand = async (text: string, ctx: Context, user: IUserDocument) => {
    const {command, param} = parseCommand(text);
    console.log("ON ADMIN", {command, param}, text)
    if (command === '/auth') {
        if (param === vars.adminSecret) {
            await userService.sendTelegramMessageToAdmins(`Пользователь ${user.userName || `${user.firstName || ''} ${user.lastName || ''}`} стал админом.`)
            user.isAdmin = true;
            await user.save()
            await ctx.reply(`Теперь ты админ, доступные команды: ${adminCommands.join(', ')}`)
            return;
        }
    }

    if (!user.isAdmin) {
        return await ctx.reply(`Даже не знаю, что на это ответить :D`);
    }

    if (command === '/users') {
        const users = await userService.getUsers();
        await ctx.reply(`Всего пользователей: ${users.length}`)
        return;
    }

    if (command === '/subs') {
        const {subs, activeSubs} = await subscriptionService.getAllWithPlans();
        const money = subs.reduce((prev, cur) => prev + cur.plan.price, 0)
        const activeMoney = activeSubs.reduce((prev, cur) => prev + cur.plan.price, 0)
        await ctx.reply(`Подписок: ${subs.length}, деняк: ${money};\nактивных: ${activeSubs.length}, деняк: ${activeMoney}`)
        return;
    }

    if (command === '/orders') {
        const orders = await orderService.getAll();
        const flats = await flatService.getAll();
        const activeFlats = flats.filter(f => f.isActive);
        const activeOrders = orders.filter(o => o.isActive)
        const uniqueFlatsCities = flats.reduce((acc: {city: string, num: number}[], cur: IFlatDocument) => {
            const index = acc.findIndex(e => e.city === cur.city);
            if (!~index) {
                acc.push({city: cur.city, num: 1})
            } else {
                acc[index].num += 1;
            }
            return acc;
        }, []).sort((a: any, b: any) => a.num > b.num ? -1 : 1)
        const uniqueOrdersCities = orders.reduce((acc: {city: string, num: number}[], cur: IOrderDocument) => {
            const index = acc.findIndex(e => e.city === cur.city);
            if (!~index) {
                acc.push({city: cur.city, num: 1})
            } else {
                acc[index].num += 1;
            }
            return acc;
        }, []).sort((a: any, b: any) => a.num > b.num ? -1 : 1)
        const uniqueFlatsStr = uniqueFlatsCities.map((e: any) => `${e.city}: ${e.num}`).join('\n');
        const uniqueOrdersStr = uniqueOrdersCities.map((e: any) => `${e.city}: ${e.num}`).join('\n');
        await ctx.reply(`*Всего предложений:* ${orders.length + flats.length}\n-\n*Квартиры:* ${flats.length} активные: ${activeFlats.length}\n${uniqueFlatsStr}\n-\n*Арендаторы:* ${orders.length} активные: ${activeOrders.length}\n${uniqueOrdersStr}`, { parse_mode: 'Markdown' })
        return;
    }

    if (command === '/matches') {
        const { ended, notEnded } = await matchService.getMatches();
        await ctx.reply(`Всего: ${ended + notEnded}\nЗакончены:${ended}\nНе закончены: ${notEnded}`, { parse_mode: 'Markdown' });
        return;
    }

    if (command === '/rmUser') {
        await user.deleteOne()
        await ctx.reply(`Пользователь удален /start`, { parse_mode: 'Markdown',
            reply_markup: { remove_keyboard: true }
          });
        return;
    }
}