import { Context } from "telegraf";
import { IUserDocument } from "../models/User.model";
import { ButtonTypes, getMessage } from "../ustils/telegram/getMessage";
import { register_renter, RENTER_REGISTER_STAGES_ORDER, RENTEE_REGISTER_STAGES_ORDER, register_rentee } from '../scenarios/register/index';
import { IInlineOption } from '../const/types';
import { Roles } from '../const/user/ROLES';
import OrderModel from "../models/Order.model";
import { userService } from '../services/user.service';
import { endRegisterStage, startRegisterStage } from "../const/scenario/REGISTER_STAGES";
import { offerStage } from "../scenarios/offer";
import { notifyAdminNewOrder, startOfferController } from "./offer.controller";
import { addressService } from "../services/address.service";
import { ILocation } from "../models/Flat.model";

const getNextStage = (user: IUserDocument) => {
    if (user.stage === startRegisterStage) {
        return RENTER_REGISTER_STAGES_ORDER[0]
    }

    const SCENARIO_OBJECT = user.role === Roles.renter ? RENTER_REGISTER_STAGES_ORDER : RENTEE_REGISTER_STAGES_ORDER;

    let index = SCENARIO_OBJECT.findIndex(k => k === user.stage);
    console.log("GET NEXT STAGE", SCENARIO_OBJECT, index)
    const nextStage = SCENARIO_OBJECT[index + 1] || endRegisterStage;
    //@ts-ignore
    if (nextStage === endRegisterStage) {
        userService.fillRegisterDocument(user, 'complete', true);
        notifyAdminNewOrder(user)
    }
    return nextStage;
}

export const addressRegisterController = async (address: string, dbField: string, user: IUserDocument) => {
    console.log('CHECK ADDRESS', address)
    const res = await addressService.getGeoData(address);
    if (!res || !res.result) {
        return [getMessage(`Адрес не найден`)]
    }

    if (res.qc_geo > 2) {
        console.log("NEED DOM", res, res.qc_geo)
        return [getMessage(`Введите адрес, с точностью до дома`)]
    }
    const { metro, result, geo_lat, geo_lon } = res;
    const data = { metro, address: result, geo_lat, geo_lon };
    await userService.fillRegisterDocument(user, dbField, data);
    const nextStageName = getNextStage(user);
    const scenario = user.role === Roles.rentee ? register_rentee : register_renter;
    const nextStage = scenario[nextStageName];
    await user.updateOne({
        $set: {
            stage: nextStageName,
        }
    })

    console.log("ADRESS NORM", res)
    return [getMessage(`Адрес успешно сохранен!`), nextStage.prepareMessage()]
}

export const textRegisterController = async (text: string | number, dbField: string, user: IUserDocument) => {
    if (text || text === 0) {
        const nextStageName = getNextStage(user);
        const scenario = user.role === Roles.rentee ? register_rentee : register_renter;
        
        await userService.fillRegisterDocument(user, dbField, text);

        const nextStage = scenario[nextStageName];

        await user.updateOne({
            $set: {
                stage: nextStageName,
            }
        })

        
        // console.log('ВСЕ ОК', nextStageName, nextStage, nextStage.prepareMessage())

        return [nextStage.prepareMessage()]
    } else {
        console.log(`Данных нет`);
        return [getMessage(`Данных нет`)]
    }
}

export const optionsRegisterController = async (options: IInlineOption[], dbField: string, user: IUserDocument, text: string) => {
    console.log('optionsRegisterController', options, dbField, text)
    if (options.find(o => o.cb === text)) {
        
        const scenario = user.role === Roles.rentee ? register_rentee : register_renter;
        await userService.fillRegisterDocument(user, dbField, text);
        const nextStageName = getNextStage(user);
        const nextStage = scenario[nextStageName];
        await user.updateOne({
            $set: {
                // [dbField]: text,
                stage: nextStageName,
            }
        })

        if(nextStage.noAnswer) {
            const userNew = await userService.findUserByChatId(user.chatId)
            const msg = nextStage.prepareMessage();
            const nextStageName2 = getNextStage(userNew);
            const nextStage2 = scenario[nextStageName2];
            
            await user.updateOne({
                $set: {
                    // [dbField]: text,
                    stage: nextStageName2,
                }
            })

            return [msg, nextStage2.prepareMessage()];
        }
        // console.log('ВСЕ ОК', nextStageName, nextStage, nextStage.prepareMessage())

        return [nextStage.prepareMessage()]
    } else {
        console.log(`Варианта ${text} нет в ${options}`);
        return [getMessage(`Выберите из предложенных вариантов`)]
    }
}

export const startRegister = async (options: IInlineOption[], dbField: string, user: IUserDocument, text: string) => {
    console.log('startRegister', options, dbField, text, Roles)
    const nextStageName = getNextStage(user);
    const scenario = user.role === Roles.rentee ? register_rentee : register_renter;
    const nextStage = scenario[nextStageName];
    if (text === Roles.renter) {
        await user.updateOne({
            $set: {
                stage: nextStageName,
            }
        })

        userService.createRenter(user);

        return [nextStage.prepareMessage()]
    }

    if (text === Roles.rentee) {
        await user.updateOne({
            $set: {
                stage: nextStageName,
            }
        })

        userService.createRentee(user);

        return [nextStage.prepareMessage()]
    }

    console.log('Start failed')

    return [getMessage(`Что-то пошло не так.`)]

}

export const addingPhoto = async (user: IUserDocument, photoId: string, isPhotoController?: boolean) => {
    if (!isPhotoController || !photoId) {
        return [getMessage('Добавьте нормальное изображение или нажмите "Готово"', ['Готово'], ButtonTypes.keyboard)]
    }
    if (photoId) {
        await userService.addPhoto(user, photoId);
        return [getMessage('Изображение сохранено.', ['Готово'], ButtonTypes.keyboard)]
    }
}

export const endAddingPhoto = async (user: IUserDocument) => {
    const nextStageName = getNextStage(user);
    const scenario = user.role === Roles.rentee ? register_rentee : register_renter;
    const nextStage = scenario[nextStageName];

    await user.updateOne({
        $set: {
            // [dbField]: text,
            stage: nextStageName,
        }
    })

    // console.log('ВСЕ ОК', nextStageName, nextStage, nextStage.prepareMessage())

    return [nextStage.prepareMessage()]
}

export const endRegister = async (options: IInlineOption[], dbField: string, user: IUserDocument, text: string) => {
    console.log('endRegister', options, dbField, text)
    if (options.find(o => o.cb === text)) {
        const nextStageName = offerStage;

        await user.updateOne({
            $set: {
                stage: nextStageName,
            }
        })

        return startOfferController(user)
    } else {
        console.log(`Варианта ${text} нет в ${options}`);
        return [getMessage(`Выберите из предложенных вариантов`)]
    }
}
