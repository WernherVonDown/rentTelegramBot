import { IInlineOption } from "../const/types";
import { IUserDocument } from "../models/User.model";
import { SettingsActions } from "../scenarios/settings";
import { getMessage, MessageTypes } from "../ustils/telegram/getMessage";
import { userService } from '../services/user.service';
import { Roles } from "../const/user/ROLES";
import { register_rentee, register_renter } from '../scenarios/register/index';
import { startRegisterStage } from '../const/scenario/REGISTER_STAGES';
import { subscriptionPlanService } from "../services/subscriptionPlan.service";
import { SubscriptionPlanKeys } from "../migrations/addSubscriptionPlans";
import { subscriptionService } from "../services/subscription.service";

export const optionsSettingsController = async (options: IInlineOption[], user: IUserDocument, text: string) => {
    if (options.find(o => o.cb === text)) {
        if (text === SettingsActions.HIDE) {
            await userService.setInactive(user);
            return [getMessage(`Объявление скрыто`)]
        } else if (text === SettingsActions.REREGISTER) {
            await userService.setInactive(user);
            await user.updateOne({
                $set: {
                    // [dbField]: text,
                    stage: startRegisterStage,
                }
            })
            const scenario = user.role === Roles.rentee ? register_rentee : register_renter;
            const res = await scenario[startRegisterStage].prepareMessage();
            return [{ message: 'Объявление скрыто', reply_markup: {remove_keyboard: true}, type: MessageTypes.text }, res]
        } else if (text === SettingsActions.SUSPEND_MATCH_LIMIT) {
            const plan = await subscriptionPlanService.getSubscriptionPlanByKey(SubscriptionPlanKeys.suspend_match_limit);
            if (plan) {
                return subscriptionService.getBuySubscriptionMessage(plan, user._id)
            }
            
        }
        return [getMessage(`Неизвестная опция ${text}`)]
    } else {
        return [getMessage(`Неизвестная опция ${text}`)]
    }
    
}