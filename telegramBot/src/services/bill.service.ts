import BillModel from "../models/Bill.model";
import { Document } from 'mongoose';

class BillService {
    async createBill (userId: string, subscriptionPlanId: string): Promise<Document> {
        const bill = new BillModel({
            user: userId,
            subscriptionPlan: subscriptionPlanId,
            createdAt: new Date().toString(),
        })
        await bill.save();
        return bill;
    }

    async getBill(billId: string) {
        return BillModel.findOne({_id: billId});
    }

    async payBill(billId: string, paymentId: string, userEmail: string) {
        const bill = await this.getBill(billId);
        if (!bill) {
            throw new Error("No bill found")
        }
        bill.paidAt = new Date().toString();
        bill.paid = true;
        bill.paymentId = paymentId;
        bill.userEmail = userEmail;
        await bill.save()
        return bill;
    }
}

export const billService = new BillService();
