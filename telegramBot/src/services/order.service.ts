import { PrioritySearchFields } from "../const/offer/PRIORITY_SEARCH_FIELDS";
import common from "../messages/common";
import { IFlatDocument } from "../models/Flat.model";
import OrderModel, { IOrderDocument } from "../models/Order.model";
import { IUserDocument } from '../models/User.model';
import { cleanEmptyObjectFields } from "../ustils/helpers/cleanEmptyObjectFields";

class OrderService {
    constructor() { }

    async updateField(_id: string, dbField: string, data: any) {
        await OrderModel.findOneAndUpdate({ _id }, { [dbField]: data })
    }

    async createOrder(ownerId: string): Promise<IOrderDocument> {
        const order = new OrderModel({
            owner: ownerId
        });
        await order.save();
        return order;
    }

    async getDocument(_id: string): Promise<IOrderDocument | null> {
        return OrderModel.findOne({ _id, isActive: true });
    }

    async getDocumentWithOwner(_id: string): Promise<IOrderDocument | null> {
        return OrderModel.findOne({ _id, isActive: true }).populate('owner');
    }

    async setInactive(_id: string) {
        await OrderModel.findOneAndUpdate({ _id }, { isActive: false });
    }

    async getPhotos(_id: string) {
        const order = await OrderModel.findOne({ _id, isActive: true })

        return order?.photos || []
    }

    getBestOption(query: any) {
        return OrderModel.find(query);
    }

    async findBestOptions(flat: IFlatDocument, seenOrders: string[], count: number): Promise<IOrderDocument[]> {
        const { city, gender, pets, smoking, livers, nation, work } = flat;
        const query = cleanEmptyObjectFields(
            { city, gender, pets, smoking, livers, nation, work }, 
            common.not_matters
        );


        let results = await OrderModel.find({
            ...query,
            isActive: true,
            complete: true,
            _id: {
                $nin: seenOrders
            }
        })

        if (!results.length) {
            const searcFields = PrioritySearchFields
            .reverse()
            .filter(f => Object.keys(query).includes(f))

            for (let field of searcFields) {
                //@ts-ignore
                delete query[field];
                results = await this.getBestOption({
                    ...query,
                    isActive: true,
                    complete: true,
                    city,
                    _id: {
                        $nin: seenOrders
                    }
                })
                if (results.length) {
                    break;
                }
            }
        }

        return results;
    }

    async addPhoto(_id: string, photoId: string) {
        await OrderModel.findOneAndUpdate({ _id }, { $addToSet: { photos: photoId } })
    }

    async getAll() {
        return OrderModel.find({});
    }
}

export const orderService = new OrderService();