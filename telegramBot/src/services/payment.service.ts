import { getPaymentLink, getPaymentYooKassLink } from "../ustils/payment/getPaymentLink";
import { billService } from "./bill.service";
import { subscriptionPlanService } from "./subscriptionPlan.service";
import { userService } from "./user.service";

class PaymentService {
    async getBillUrl (userId: string, subscriptionPlanId: string) {
        const user = await userService.getUserById(userId);
        if (!user) {
            throw new Error("No user found")
        }
        const subscriptionPlan = await subscriptionPlanService.getSubscriptionPlanById(subscriptionPlanId);
        if (!subscriptionPlan) {
            throw new Error("No subscriptionPlan found")
        }

        const bill = await billService.createBill(userId, subscriptionPlanId);
        const link = await getPaymentYooKassLink({
            billId: bill._id, 
            price: subscriptionPlan.price, 
            userId, userName: user.userName || `${user.firstName} ${user.lastName}`,
            description: 'Оплата подписки'
        })
        if (!link) {
            throw new Error("No confirmation link")
        }
        return link;
        // return getPaymentLink({orderId: bill._id, price: subscriptionPlan.price, currency: subscriptionPlan.currency, name: subscriptionPlan.title})
    }
}

export const paymentService = new PaymentService();
