import { vars } from "../config/vars";
import { SubscriptionPlanKeys } from "../migrations/addSubscriptionPlans";
import SubscriptionModel from "../models/Subscription.model";
import { ISubscriptionPlan } from "../models/SubscriptionPlan.model";
import { getBillLink } from "../ustils/payment/getBillLink";
import { getMessage } from "../ustils/telegram/getMessage";
import { makeOptionUrl } from "../ustils/telegram/makeOption";
import { subscriptionPlanService } from "./subscriptionPlan.service";

const pay_url = `https://merchant.intellectmoney.ru/ru?eshopId=462963&orderId=${Date.now()}&UserField_1=1488&UserFieldName_1=userId&serviceName=buy_shit&recipientAmount=10&recipientCurrency=TST&user_email=aleekseevbursche@yandex.ru&preference=bankcard&merchantReceipt=%7B%22inn%22:%22780531351060%22,%22group%22:%22Main%22,%22content%22:%7B%22type%22:1,%22positions%22:%5B%7B%22quantity%22:1.000,%22price%22:10,%22tax%22:1,%22text%22:%22%D0%A1%D0%BF%D0%B8%D1%87%D0%BA%D0%B8%22%7D%5D,%22customerContact%22:%22aleekseevbursche@yandex.ru%22%7D%7D`;

class SuscriptionService {
    async checkMatchesLimit(userId: string, matches: number): Promise<{success: boolean, messages?: any[]}> {
        const plan = await subscriptionPlanService.getSubscriptionPlanByKey(SubscriptionPlanKeys.suspend_match_limit);
        if  (!plan) {
            console.log("Error; No plan found");
            return {success: false};
        }

        const hasSub = await this.isUserSubscriptionActiveById(userId, plan._id);

        if (hasSub) {
            console.log("User has sub", SubscriptionPlanKeys.suspend_match_limit);
            return {success: true};
        }

        console.log("PLAN URL", getBillLink(plan._id, userId), vars.apiUrl, matches)
        if (matches >= vars.maxMatches) {
            return {
                messages: this.getBuySubscriptionMessage(plan, userId),
                success: false,        
            }
        }

        return {success: false}
    }

    getBuySubscriptionMessage (plan: ISubscriptionPlan, userId: string) {
        return [getMessage("Чтобы уберечь пользователей от спама, мы ограничили лимит откликов за сутки", [makeOptionUrl(`Снять лимит на 24 часа за ${plan.price}₽`, getBillLink(plan._id, userId))])]
    }

    async getUserSubscriptonByKey(userId: string, subId: string, active?: boolean) {
        return SubscriptionModel.find({
            user: userId,
            plan: subId,
            active,
        })
    }

    async isUserSubscriptionActiveById(userId: string, subId: string) {
        const subs = await this.getUserSubscriptonByKey(userId, subId, true);
        if (!subs.length) return false;
        const res = (await Promise.all(subs.map(async s => {
            const expTime = new Date(s.expiresAt).getTime()
            if (expTime < Date.now()) {
                s.active = false;
                await s.save();
                return false
            }
            return s;
        }))).find((s) => s);

        return res;
    }

    async getAllWithPlans() {
        const subs = await SubscriptionModel.find().populate('plan')
        const activeSubs = subs.filter(s => {
            const expTime = new Date(s.expiresAt).getTime()
            if (expTime < Date.now()) {
                return false
            }
            return true;
        })
        return {subs, activeSubs}
    }

    async createUserSubscription(userId: string, subscriptionPlanId: string) {
        const subPlan = await subscriptionPlanService.getSubscriptionPlanById(subscriptionPlanId);
        if (!subPlan) {
            throw new Error(`createUserSubscription; error no subPlan userId=${userId}, subPlanId=${subscriptionPlanId}`);
        }
        console.log("CREATE SUB; PLAN", subPlan)
        const sub = new SubscriptionModel({
            user: userId,
            plan: subscriptionPlanId,
            createdAt: new Date().toString(),
            expiresAt: new Date(Date.now() + subPlan.period).toString(),
            active: true,
        })
        await sub.save()
        return sub;
    }
}

export const subscriptionService = new SuscriptionService();
