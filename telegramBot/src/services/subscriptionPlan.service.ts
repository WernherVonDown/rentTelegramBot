import SubscriptionPlanModel, { ISubscriptionPlan } from "../models/SubscriptionPlan.model";
import { userService } from "./user.service";

class SubscriptionPlanService {
    async getSubscriptionPlanById(subId: string):Promise<ISubscriptionPlan | null> {
        return SubscriptionPlanModel.findOne({ _id: subId });
    }

    async getSubscriptionPlanByKey(key: string):Promise<ISubscriptionPlan | null> {
        return SubscriptionPlanModel.findOne({ key });
    }
}

export const subscriptionPlanService = new SubscriptionPlanService();
