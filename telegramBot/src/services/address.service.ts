import axios from "axios";
import dotenv from 'dotenv';
dotenv.config();
const token = process.env.DADATA_TOKEN || '';
const secret = process.env.DADATA_SECRET || '';

const axiosInstance = axios.create({
    headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Token " + token,
        "X-Secret": secret,
    }
})

// axios.defaults.headers.common["Content-Type"] = "application/json";
// axios.defaults.headers.common["Accept"] = "application/json";
// axios.defaults.headers.common["Authorization"] = "Token " + token;
// axios.defaults.headers.common["X-Secret"] = secret;

class AddressService {
    constructor() { }

    async getGeoData(query: string) {
        const url = "https://cleaner.dadata.ru/api/v1/clean/address";
        try {
            const res = await axiosInstance.post(url, [query]);
            // console.log("GEO", res.data)
            return res.data[0] || undefined;
        } catch (error) {
            console.log('AddressService.completeAddress error', error)
        }
    }

    async completeAddress(query: string) {
        try {
            const url = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address";
            const res = await axiosInstance.post(url, { query: query }
                // {body: JSON.stringify({ query: query })}
            )
            // console.log("RES", res.data)
            return res.data?.suggestions || []
        } catch (error) {
            console.log('AddressService.completeAddress error', error)
        }
    }
}

export const addressService = new AddressService();