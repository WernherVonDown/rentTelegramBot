import { PrioritySearchFields } from "../const/offer/PRIORITY_SEARCH_FIELDS";
import common from "../messages/common";
import FlatModel, { IFlatDocument, ILocation } from "../models/Flat.model";
import { IOrderDocument } from "../models/Order.model";
import { cleanEmptyObjectFields } from "../ustils/helpers/cleanEmptyObjectFields";

class FlatService {
    constructor() { }

    async updateField(_id: string, dbField: string, data: any) {
        await FlatModel.findOneAndUpdate({_id}, {[dbField]: data})
    }

    async createFlat(ownerId: string):Promise<IFlatDocument> {
        const flat = new FlatModel({
            owner: ownerId
        });
        await flat.save();
        return flat;
    }

    async getDocument(_id: string): Promise<IFlatDocument | null> {
        return FlatModel.findOne({_id, isActive: true });
    }

    async getDocumentWithOwner(_id: string): Promise<IOrderDocument | null> {
        return FlatModel.findOne({_id, isActive: true }).populate('owner');
    }

    async setInactive(_id: string) {
        await FlatModel.findOneAndUpdate({ _id }, { isActive: false });
    }

    getBestOption(query: any) {
        return FlatModel.find(query);
    }

    async getAll() {
        return FlatModel.find({});
    }

    async findBestOptions(order: IOrderDocument, seenFlats: string[], count: number): Promise<IOrderDocument[]> {
        const { city, gender, pets, smoking, livers, nation, work } = order;
        const query = cleanEmptyObjectFields(
            { city, gender, pets, smoking, livers, nation, work }, 
            common.not_matters
        );


        let results = await FlatModel.find({
            ...query,
            isActive: true,
            complete: true,
            _id: {
                $nin: seenFlats
            }
        })

        if (!results.length) {
            const searcFields = PrioritySearchFields
            .reverse()
            .filter(f => Object.keys(query).includes(f))

            for (let field of searcFields) {
                //@ts-ignore
                delete query[field];
                results = await this.getBestOption({
                    ...query,
                    isActive: true,
                    complete: true,
                    city,
                    _id: {
                        $nin: seenFlats
                    }
                })
                if (results.length) {
                    break;
                }
            }
        }

        return results;
    }

    async getPhotos(_id: string) {
        const flat = await FlatModel.findOne({_id, isActive: true})

        return flat?.photos || []
    }

    async addPhoto (_id: string, photoId: string) {
        await FlatModel.findOneAndUpdate({_id, isActive: true}, {$addToSet: {photos: photoId}})
    }
}

export const flatService = new FlatService();