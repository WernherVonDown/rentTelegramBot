import MatchModel from "../models/Match.model";
import { getMessage } from "../ustils/telegram/getMessage";
import { subscriptionService } from "./subscription.service";

class MatchService {
    async startMatch(
        { initiator, target, initiatorOrder, targetOrder }:
            { initiator: string, target: string, initiatorOrder: string, targetOrder: string }
    ): Promise<any[] | undefined> {
        const matches = await MatchModel.find({
            startTime: { $gt: new Date().setHours(0, 0, 0) },
            initiator,
            counted: true,
        })

        const {success, messages} = await subscriptionService.checkMatchesLimit(initiator, matches.length);
        if (messages) {
            return messages;
        }
        

        await MatchModel.create({
            initiator,
            target,
            initiatorOrder,
            targetOrder,
            completed: false,
            startTime: Date.now(),
            counted: !success
        })
    }

    async endMatch(
        { initiator, target, initiatorOrder, targetOrder }:
            { initiator: string, target: string, initiatorOrder: string, targetOrder: string }
    ) {
        const match = await MatchModel.findOne({
            initiator,
            target,
            initiatorOrder,
            targetOrder,
        })

        console.log("END MATCH", { initiator, target, initiatorOrder, targetOrder })

        if (match) {
            match.startTime = Date.now();
            match.completed = true;
            await match.save()
        } else {
            console.log("MATCH не найден")
        }
    }

    async getMatches() {
        const matches = await MatchModel.find({});
        const ended = matches.filter(m => m.completed).length;
        const notEnded = matches.filter(m => !m.completed).length;
        return {ended, notEnded}
    }
}

export const matchService = new MatchService();