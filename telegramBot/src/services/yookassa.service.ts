import { ICreatePayment, YooCheckout } from "@a2seven/yoo-checkout";
import { vars } from "../config/vars";

class YookassaService {
    private yooKassa: YooCheckout = new YooCheckout({
        shopId: vars.yookassa.shopId,
        secretKey: vars.yookassa.secretKey
    })

    async capture(paymentId: string) {
        return this.yooKassa.capturePayment(paymentId, {});
    }

    async createPayment({billId, userId, userName, price, description = ''}: {billId: string, userId: string, userName: string, price: string | number, description?: string}) {
        const createPayload: ICreatePayment = {
            amount: {
                value: `${price}`,
                currency: 'RUB'
            },
            payment_method_data: {
                type: 'bank_card'
            },
            confirmation: {
                type: 'redirect',
                return_url: 'test'
            },
            description: description || `Покупка ${billId}`,
            metadata: {
                billId: billId,
                userId: userId,
                userName: userName,
            }
        };
        return this.yooKassa.createPayment(createPayload);
    }
}

export const yookassaService = new YookassaService();
