import UserModel from "../models/User.model";
import { IUserDocument } from '../models/User.model';
import { Roles } from '../const/user/ROLES';
import { startRegisterStage } from "../const/scenario/REGISTER_STAGES";
import { orderService } from "./order.service";
import { flatService } from "./flat.service";
import { sendMessage, sendMessages } from "../app";

const ADMIN_USERS = [
    'Ivan_mrnk',
    'EdwardPoo',
    // 'sophienekhay',
    'lavandv',
]

class UserService {
    constructor() { }

    async getUser(userData: any): Promise<IUserDocument> {
        const { firstName, lastName, userName, chatId, languageCode } = userData;
        let user = await UserModel.findOne({ chatId });
        if (!user) {
            user = new UserModel({
                firstName,
                lastName,
                userName,
                chatId,
                languageCode,
                stage: startRegisterStage,
                isAdmin: ADMIN_USERS.includes(userName),
            });

            await user.save()
        }

        return user;
    }

    async getUserById(userId: string) {
        return UserModel.findOne({ _id: userId });
    }

    async sendTelegramMessage(userId: string, message: string) {
        const user = await this.getUserById(userId);
        if (!user) throw new Error(`sendTelegramMessage; error, no user found userId=${userId}, text=${message}`);

        await sendMessage({chatId: user.chatId, message })
    }

    async getAdmins(): Promise<IUserDocument[]> {
        return UserModel.find({ isAdmin: true });
    }

    async sendTelegramMessageToAdmins(message: string, msgData = {}) {
        const admins = await this.getAdmins();
        if (Object.keys(msgData)) {
            return Promise.all(admins.map(({chatId}) => sendMessages([{ message, ...msgData }], chatId)))
        }

        return Promise.all(admins.map(({chatId}) => sendMessage({ chatId, message, ...msgData })))
    }

    async findUserByChatId(chatId: string) {
        return UserModel.findOne({ chatId });
    }

    async addSeenFlats (user: IUserDocument, seenFlats: string[]) {
        await user.updateOne({
            $addToSet: { seenFlats: { $each: seenFlats}}
        })
    }

    async addSeenOrders (user: IUserDocument, seenOrders: string[]) {
        await user.updateOne({
            $addToSet: { seenOrders: { $each: seenOrders}}
        })
    }

    async addPhoto(user: IUserDocument, photoId: string) {
        if (user.role === Roles.renter) {
            await orderService.addPhoto(user.order, photoId);
        } else if (user.role === Roles.rentee) {
            await flatService.addPhoto(user.flat, photoId);
        }
    }

    async getBestOptions(user: IUserDocument, count: number = Infinity) {
        if (user.role === Roles.renter) {
            const order = await orderService.getDocument(user.order);
            if (order) {
                const result = await flatService.findBestOptions(order, user.seenFlats, count);
                await this.addSeenFlats(user, result.map(r => r._id).slice(0, count));
                return result.slice(0, count);
            }
        } else if (user.role === Roles.rentee) {
            const flat = await flatService.getDocument(user.flat);
            if (flat) {
                const result = await orderService.findBestOptions(flat, user.seenOrders, count);
                await this.addSeenOrders(user, result.map(r => r._id).slice(0, count));
                return result.slice(0, count);
            }
        }

        return []
    } 

    async createRentee(user: IUserDocument) {
        const flat = await flatService.createFlat(user._id)
        await user.updateOne({
            $set: {
                role: Roles.rentee,
                flat: flat._id
            }
        })
    }

    async setInactive(user: IUserDocument) {
        if (user.role === Roles.renter) {
            await orderService.setInactive(user.order);
        } else if (user.role === Roles.rentee) {
            await flatService.setInactive(user.flat);
        }
    }

    async fillRegisterDocument(user: IUserDocument, dbField: string, data: any) {
        if (user.role === Roles.renter) {
            await orderService.updateField(user.order, dbField, data);
        } else if (user.role === Roles.rentee) {
            await flatService.updateField(user.flat, dbField, data);
        }
    }

    async createRenter(user: IUserDocument) {
        const order = await orderService.createOrder(user._id);
        console.log('SET ORDER', order._id)
        await user.updateOne({
            $set: {
                role: Roles.renter,
                order: order._id
            }
        })
    }

    async getUsers() {
        return UserModel.find({});
    }
}

export const userService = new UserService();