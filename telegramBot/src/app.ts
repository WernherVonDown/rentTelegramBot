import { Context, Markup, Router, Telegraf, TelegramError } from 'telegraf'
import { telegrafThrottler } from 'telegraf-throttler';
import _ from 'lodash';
import axiosThrottle from 'axios-request-throttle';
import messages from './messages';
import authMiddleware from './middlewares/auth.middleware';
import { register_rentee, register_renter } from './scenarios/register/';
import { IUserDocument } from './models/User.model';
import { startRegisterStage, endRegisterStage, REGISTER_STAGES } from './const/scenario/REGISTER_STAGES';
import { offerStage, offer_scenario } from './scenarios/offer/index';

const token = process.env.TELEGRAM_API_TOKEN || '';

const bot = new Telegraf(token);

bot.use(Telegraf.log())

const outConfig = {
  minTime: 300,                    // Wait this many milliseconds to be ready, after a job
  reservoir: 30,                  // Number of new jobs that throttler will accept at start
  reservoirRefreshAmount: 30,     // Number of jobs that throttler will accept after refresh
  reservoirRefreshInterval: 1000, // Interval in milliseconds where reservoir will refresh
};

bot.use(authMiddleware);
const throttler = telegrafThrottler({
  out: outConfig,
  inKey: 'chat',
});
bot.use(throttler);

import axios, { Method, CancelToken } from 'axios';
import { endAddingPhoto } from './controllers/register.controller';
import { MessageTypes } from './ustils/telegram/getMessage';
import { Roles } from './const/user/ROLES';
import { orderService } from './services/order.service';
import { flatService } from './services/flat.service';
import { getContacts } from './controllers/offer.controller';
import { startBoot } from './boot';
import common from './messages/common';
import { common_settings, SETTINGS_PREFIX } from './scenarios/settings/index';
import { matchService } from './services/match.service';
import { IFlatDocument } from './models/Flat.model';
import { IOrderDocument } from './models/Order.model';
import { isAdminCommand } from './ustils/helpers/isAdminCommand';
import { onAdminCommand } from './controllers/admin.controller';
import { userService } from './services/user.service';
import { vars } from './config/vars';
import errorsStore from './ustils/errorsStore';
import { sendAlarmMessage } from './ustils/sendAlarmMessage';

interface IArgs {
  method: Method;
  url: string;
  data: any;
  params?: Record<string, any>;
  headers?: Record<string, any>;
  cancelToken?: CancelToken;
  onUploadProgress?: (progressEvent: any) => void;
  onDownloadProgress?: (progressEvent: any) => void;
}

axiosThrottle.use(axios, { requestsPerSecond: 5 });

export const sendHttpRequest = (args: IArgs) => {
  const {
    url,
    method,
    data,
    params,
    headers,
    cancelToken,
    onUploadProgress,
    onDownloadProgress,
  } = args;
  return axios({
    url,
    method,
    data,
    params,
    headers,
    cancelToken,
    onUploadProgress,
    onDownloadProgress,
  });
};


export async function sendMessage({ chatId, message, reply_markup, parse_mode = 'Markdown' }: { chatId: string, message: string, reply_markup?: any, parse_mode?: string }) {
  try {
    await sendHttpRequest({
      url: `https://api.telegram.org/bot${token}/sendMessage`,
      method: 'post',
      data: {
        chat_id: chatId,
        text: message,
        reply_markup,
        parse_mode,
      },
    });
  } catch (error: any) {
    console.log("SEND MESSAGE ERROR 1", error.response.data, error.response.data?.error_code, error.response.data?.parameters?.retry_after)
    const data = error.response.data;
    if (data.error_code === 429 && data?.parameters?.retry_after) {
      errorsStore.addErrorMsg(`fucking tg limits, should retry after ${data?.parameters?.retry_after}`)
      setTimeout(() => {
        console.log("RETRY SHIIIT AFTER", data?.parameters?.retry_after)
        sendMessage({ chatId, message, reply_markup, parse_mode })
      }, data?.parameters?.retry_after * 1000)
    } else {
      errorsStore.addErrorMsg(`HZ error, after ${message}`)
    }
  }

};

const sendPhotos = async (
  { chatId, message, reply_markup, parse_mode = 'Markdown', photos, reply_to_message_id }:
    {
      chatId: string, message?: string, reply_markup?: any, parse_mode?: string,
      reply_to_message_id?: number,
      photos: { type: string, media: string, caption?: string, reply_markup?: any }[]
    }) => {
  try {
    photos[0].caption = message;
    // photos[0].reply_markup = reply_markup;
    await sendHttpRequest({
      url: `https://api.telegram.org/bot${token}/sendMediaGroup`,
      method: 'post',
      data: {
        chat_id: chatId,
        // caption: message,
        reply_markup,
        parse_mode,
        media: photos,
        reply_to_message_id
      },
    });
  } catch (error: any) {
    console.log("SEND MESSAGE ERROR 2", error, error.response, error.response.data, error.messsage, { chatId, message }, reply_markup)
    const data = error.response.data;
    if (data.error_code === 429 && data?.parameters?.retry_after) {
      errorsStore.addErrorMsg(`fucking tg limits, should retry after ${data?.parameters?.retry_after}`)
      setTimeout(() => {
        sendPhotos({ chatId, message, reply_markup, parse_mode, photos, reply_to_message_id })
      }, data?.parameters?.retry_after * 1000)
    }
  }
};

const sendPhoto = async (
  { chatId, message, reply_markup, parse_mode = 'Markdown', photo }:
    {
      chatId: string, message: string, reply_markup?: any, parse_mode?: string,
      photo: any
    }) => {
  try {
    await sendHttpRequest({
      url: `https://api.telegram.org/bot${token}/sendPhoto`,
      method: 'post',
      data: {
        chat_id: chatId,
        caption: message,
        reply_markup,
        parse_mode,
        photo: photo,
      },
    });
  } catch (error: any) {
    console.log("SEND MESSAGE ERROR 3", error, error.response, error.response.data, error.messsage, { chatId, message }, reply_markup)
    const data = error.response.data;
    if (data.error_code === 429 && data?.parameters?.retry_after) {
      errorsStore.addErrorMsg(`fucking tg limits, should retry after ${data?.parameters?.retry_after}`)
      setTimeout(() => {
        sendPhoto({ chatId, message, reply_markup, parse_mode, photo })
      }, data?.parameters?.retry_after * 1000)
    } else {
      errorsStore.addErrorMsg(`HZ error, after ${message}`)
    }
  }
};

const sendDocument = async (
  { chatId, message, reply_markup, parse_mode = 'Markdown', document }:
    {
      chatId: string, message: string, reply_markup?: any, parse_mode?: string,
      document: any
    }) => {
  try {
    await sendHttpRequest({
      url: `https://api.telegram.org/bot${token}/sendDocument`,
      method: 'post',
      data: {
        chat_id: chatId,
        caption: message,
        reply_markup,
        parse_mode,
        document: document,
      },
    });
  } catch (error: any) {
    console.log("SEND MESSAGE ERROR 4", error, error.response, error.response.data, error.messsage, { chatId, message }, reply_markup)
    const data = error.response.data;
    if (data.error_code === 429 && data?.parameters?.retry_after) {
      setTimeout(() => {
        sendDocument({ chatId, message, reply_markup, parse_mode, document })
      }, data?.parameters?.retry_after * 1000)
    }
  }
};

export const sendMessages = async (msgs: any[], chatId: string) => {
  console.log("SEND MSGS", msgs)

  for (let m of msgs) {
    const type = m.type;
    delete m.type;
    if (type === MessageTypes.photo) {
      await sendPhoto({ chatId, ...m });
      return;
    }

    if (type === MessageTypes.media_group) {
      await sendDocument({ chatId, ...m })
      return;
    }
    await sendMessage({ chatId, ...m })
  }
}

bot.on("callback_query", async (ctx: Context) => {
  const user: IUserDocument = ctx.state.user;
  const text = ctx.state.inlineData;
  const scenario = user.role === Roles.rentee ? register_rentee : register_renter;
  const settingsRegExp = new RegExp(`${SETTINGS_PREFIX}_(.+)`)

  try {
    const settings = text.match(settingsRegExp);
    if (settings) {
      const res = await common_settings.controller(user, text);
      await sendMessages(res, user.chatId)
    }

    const match_variant = text.match(/match_(.+)/);
    if (match_variant) {
      const documentId = match_variant[1];
      let matcherDocument;
      let matcheeDocument;
      //
      if (user.role === Roles.rentee) {
        matcheeDocument = await orderService.getDocumentWithOwner(documentId);
        matcherDocument = await flatService.getDocumentWithOwner(user.flat);
      } else if (user.role === Roles.renter) {
        matcheeDocument = await flatService.getDocumentWithOwner(documentId);
        matcherDocument = await orderService.getDocumentWithOwner(user.order);
      }
      const messageId = ctx?.callbackQuery?.message?.message_id;

      if (matcherDocument && matcherDocument.owner) {
        if (matcheeDocument) {
          const matcheeUser = matcheeDocument.owner as IUserDocument;
          await matchService.endMatch({
            //@ts-ignore
            initiator: matcheeUser._id,
            target: user._id,
            initiatorOrder: matcheeDocument._id,
            targetOrder: matcherDocument._id,
          })
          const res = await offer_scenario[offerStage].matchController(matcherDocument, matcheeDocument);

          await ctx.reply(`Контакты пользователя:\n${getContacts(matcheeDocument)}`, { reply_to_message_id: messageId })
          await sendMessages(res, matcheeUser.chatId);
        } else {
          await ctx.reply('Ваше предложение не найдено в базе, попробуйте заново пройти регистрацию /start')
        }
      } else {
        await ctx.reply('Пользователь был удалён')
      }

      return;
    }

    const send_location = text.match(/location_(.+)_(.+)/);

    if (send_location) {
      const lat = send_location[1];
      const lon = send_location[2];
      const messageId = ctx?.callbackQuery?.message?.message_id;
      if (lat && lon) {
        await ctx.replyWithLocation(lat, lon, { reply_to_message_id: messageId });
        return;
      }

      await ctx.reply(`Не смогли распознать координаты :(`)
      return;
    }


    const choose_variant = text.match(/choose_(.+)/);
    if (choose_variant) {
      const documentId = choose_variant[1];
      let chooserDocument;
      let chooseeDocument;

      if (user.role === Roles.rentee) {
        chooseeDocument = await orderService.getDocumentWithOwner(documentId);
        chooserDocument = await flatService.getDocument(user.flat);
      } else if (user.role === Roles.renter) {
        chooseeDocument = await flatService.getDocumentWithOwner(documentId);
        chooserDocument = await orderService.getDocument(user.order);
      }

      if (chooseeDocument && chooseeDocument.owner) {
        if (chooserDocument) {
          const chooseeUser = chooseeDocument.owner as IUserDocument
          const errorMsgs = await matchService.startMatch({
            initiator: user._id,
            target: chooseeUser._id,
            initiatorOrder: chooserDocument._id,
            targetOrder: chooseeDocument._id,
          })

          if (errorMsgs) {
            await sendMessages(errorMsgs, user.chatId);
            return;
          }

          const res = await offer_scenario[offerStage].chooseController(chooserDocument, chooseeDocument);

          await ctx.reply(`Отправили ваше предолжение пользователю`)
          await sendMessages(res, chooseeUser.chatId);

        } else {
          await ctx.reply('Ваше предложение не найдено в базе, попробуйте заново пройти регистрацию /start')
        }
      } else {
        await ctx.reply('Пользователь был удалён')
      }
      return;
    }
    // console.log('STAGE', user.stage, register_renter)
    const see_photos = text.match(/photos_(.+)/);
    if (see_photos) {

      const documentId = see_photos[1];
      //@ts-ignore
      const messageId = ctx?.callbackQuery?.message?.message_id;
      console.log("MATCHED SEE PHOTOS", see_photos, documentId, messageId, user.role)
      let photos = [];
      if (user.role === Roles.rentee) {
        photos = await orderService.getPhotos(documentId)
      } else if (user.role === Roles.renter) {
        photos = await flatService.getPhotos(documentId);
      }

      if (user.isAdmin && !photos.length) {//REFACTOR SHIT
        if (user.role === Roles.renter) {
          photos = await orderService.getPhotos(documentId)
        } else if (user.role === Roles.rentee) {
          photos = await flatService.getPhotos(documentId);
        }
      }

      if (!photos.length) {
        return await ctx.reply('Фото не найдены')
      }

      sendPhotos({
        chatId: user.chatId,
        // message: 'Fuck off',
        //@ts-ignore
        reply_to_message_id: messageId,
        photos: photos.map((p: string) => ({ type: 'photo', media: p }))
      })

      return;
    }
    if (scenario[user.stage]) {
      const res = await scenario[user.stage].controller(user, text);
      console.log("REPLY FOR STAGE", user.stage, res)
      //@ts-ignore
      // await Promise.all(res.map(r => ctx.reply(...r)))
      await sendMessages(res, user.chatId);
    }
  } catch (error: any) {
    if (error.code === 429 && error.response?.parameters?.retry_after) {
      errorsStore.addErrorMsg(`fucking tg limits, should retry after ${error.response?.parameters?.retry_after}`)
      setTimeout(async () => {
        await ctx.reply(`Произошла ошибка, попробуйте обратиться к ${vars.helpContact} или повторите попытку`).catch(e => console.log("error reply", e))
      }, error.response.parameters.retry_after * 1000)
    } else {
      errorsStore.addErrorMsg(`HZ error, after ${text}`)
    }
    console.log("error inline", error, error.code, error.on?.payload, error.data, error.response, error.response.parameters.retry_after, text, user)
  }

})

bot.on('contact', async (ctx) => {
  const user: IUserDocument = ctx.state.user;
  const scenario = user.role === Roles.rentee ? register_rentee : register_renter;
  if (scenario[user.stage]) {
    await ctx.reply('Номер сохранен!',
      {
        reply_markup: { remove_keyboard: true }
      });
    const res = await scenario[user.stage].controller(user, ctx.message.contact.phone_number);
    await sendMessages(res, user.chatId);
  }

})

bot.on("photo", async (ctx) => {
  const user: IUserDocument = ctx.state.user;
  const scenario = user.role === Roles.rentee ? register_rentee : register_renter;
  console.log("EEEEE", ctx.state.photo)
  if (scenario[user.stage] && user.stage === REGISTER_STAGES.photo) {
    const res = await scenario[user.stage].controller(user, ctx.state.photo, true);
    await sendMessages(res, user.chatId);
  }
})

bot.hears(/photos_(.+)/, ctx => {
  console.log('AAAAAAAAAA MATCH', ctx.match)
})

bot.inlineQuery('Посмотреть все фото', ctx => {
  console.log("ALLLLL", ctx.state)
})

bot.on('chosen_inline_result', ({ chosenInlineResult }) => {
  console.log('chosen inline result', chosenInlineResult)
})


bot.hears(/Пропустить|Готово/, async (ctx) => {
  console.log("ПРОПУСТИТЬ ЛОЛ")
  const user: IUserDocument = ctx.state.user;
  const scenario = user.role === Roles.rentee ? register_rentee : register_renter;
  if (scenario[user.stage] && user.stage === REGISTER_STAGES.photo) {
    await ctx.reply('Изменения сохранены',
      {
        reply_markup: { remove_keyboard: true }
      });

    const res = await scenario[user.stage].endController(user);
    await sendMessages(res, user.chatId);
  }
})

bot.on('message', async (ctx) => {
  const user: IUserDocument = ctx.state.user;


  //@ts-ignore
  const text = ctx.message?.text || '';

  console.log('MESSAGE', text)

  try {
    if (isAdminCommand(text)) {
      return onAdminCommand(text, ctx, user);
    }

    if (text === '/help') {
      await ctx.reply(messages.help);
      return
    }

    if (text === '/start') {
      if (user.isAdmin) {
        await ctx.reply(messages.start);
      } else {
        await ctx.reply(messages.start,
          {
            // reply_markup: { remove_keyboard: true }
          });
      }
      if (user.stage !== startRegisterStage) {
        return;
      }
    }

    const scenario = user.role === Roles.rentee ? register_rentee : register_renter;

    if (user.stage === startRegisterStage) {
      const res = await scenario[startRegisterStage].prepareMessage();
      await sendMessages([res], user.chatId);
      return
    }

    const register_stage = scenario[user.stage];

    if (register_stage && register_stage.options) {
      await ctx.reply(messages.choose_options)
      return;
    }

    if (register_stage && !register_stage.options) {
      const res = await register_stage.controller(user, text);
      await sendMessages(res, user.chatId)
      return;
    }

    if (user.stage === offerStage && text === common.more_variants) {
      const res = await offer_scenario[user.stage].controller(user);
      await sendMessages(res, user.chatId);
      return;
    }

    if (user.stage === offerStage && text === common.settings) {
      const res = common_settings.prepareMessage()
      await sendMessages([res], user.chatId);
      return;
    }

    await ctx.reply("Даже не знаю, что на это ответить :D");
  } catch (error: any) {
    if (error.code === 429 && error.response?.parameters?.retry_after) {
      errorsStore.addErrorMsg(`fucking tg limits, should retry after ${error.response?.parameters?.retry_after}`)
      setTimeout(async () => {
        await ctx.reply(`Произошла ошибка, попробуйте обратиться к ${vars.helpContact} или повторите попытку`).catch(e => console.log("error reply", e))
      }, error.response.parameters.retry_after * 1000)
    } else {
      errorsStore.addErrorMsg(`HZ error, after ${text}`)
    }
    console.log("error", error, error.code, error.on?.payload, error.data, error.response, error.response.parameters.retry_after, text, user)
  }
})


startBoot().then(async () => {
  await bot.launch().catch(() => console.log("СЛУЧИЛАСЬ ОШИБКА НАХУЙ 2"));
  bot.catch(() => console.log("СЛУЧИЛАСЬ ОШИБКА НАХУЙ"))
}).catch(e => console.log("ERROR", e))

process.on('unhandledRejection', async (error: any) => {
  console.log('unhandledRejection BLYA', error.message);
  await sendAlarmMessage(`[${vars.envTag}] unhandledRejection: ${error.message}`);
});
