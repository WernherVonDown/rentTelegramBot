import { Context } from "telegraf";
import { userService } from '../services/user.service';
import errorsStore from "../ustils/errorsStore";

const getUserData = (ctx: Context) => {
  const source = ctx.message || ctx.callbackQuery
  // console.log('LOLOLOLO', ctx.message
  // , ctx.callbackQuery
  // )
  if (ctx.message && ctx.message.from) {
    return ctx.message.from
  }

  if (ctx.callbackQuery && ctx.callbackQuery.from) {
    return ctx.callbackQuery.from
  }

  console.log("LOLOLOLO NOT FAUND", ctx)
  return {}
}


export default async (ctx: Context, next: () => void) => {
  try {
    // const oldCallApi = ctx.telegram.callApi.bind(ctx.telegram);
    // const newCallApi: typeof ctx.telegram.callApi = async function newCallApi(this: typeof ctx.telegram, method, payload, { signal } = {}) {
    //   console.log("CALL API", method, payload, { signal })
     
    //   const res = await  oldCallApi(method, payload, { signal })
    //   console.log("CALL API RES", res)
    //   return res;
    // }
    // ctx.telegram.callApi = newCallApi.bind(ctx.telegram);
  //@ts-ignore
  const source = ctx.message || ctx.callbackQuery || {};
    ctx.state.hello = 'world'

    // getUserData(ctx)
    //@ts-ignore
    // console.log('SOURCE', source?.message, !!ctx.callbackQuery, !!ctx.message)
    //@ts-ignore
    ctx.state.inlineData = ctx.callbackQuery?.data;
    //@ts-ignore
    ctx.state.photo = ctx?.message?.photo && ctx?.message?.photo[0].file_id;
    const {
      first_name: firstName ,
      last_name: lastName,
      username: userName ,
      id: chatId,
      language_code: languageCode,
      //@ts-ignore
    } = (getUserData(ctx) || ctx.from) as any;
    if (!chatId) {
      console.log("No chat id error", { firstName, lastName, userName, chatId, languageCode })
      throw "No chat id"
    }
     ctx.state.user = await userService.getUser(
      { firstName, lastName, userName, chatId, languageCode }
    )
    next()
  } catch (error) {
    errorsStore.addErrorMsg(`Error in auth.middleware`)
    await ctx.reply("Произошла ошибка, наши контакты /help").catch(e => console.log('Reply error 2', e))
  }
  }