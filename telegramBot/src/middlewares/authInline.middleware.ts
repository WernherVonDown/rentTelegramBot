import { Context } from "telegraf";
import { userService } from '../services/user.service';

export default async (ctx: Context, next: () => void) => {
    //@ts-ignore
    const chatId = ctx?.update?.inline_query?.from?.id;
    const user = await userService.findUserByChatId(chatId || 'no');
    console.log("MIDDLE", chatId, user)
    ctx.state.user = user;
    next();
}