import SubscriptionPlanModel from "../models/SubscriptionPlan.model";
import msTime from "../ustils/helpers/msTime";

export enum SubscriptionPlanKeys {
    suspend_match_limit = 'suspend_match_limit',
}

const subscriptionPlans = [
    {
        price: 49,
        currency: undefined,
        title: 'suspend match limit',
        description: 'Снять ограничение на 24 часа',
        benifits: [],
        discount: 0,
        key: SubscriptionPlanKeys.suspend_match_limit,
        discontEndDate: undefined,
        period: msTime.hours(24),

    }
]


export const addSubscriptionPlans = async () => {
    await SubscriptionPlanModel.insertMany(subscriptionPlans);
}
