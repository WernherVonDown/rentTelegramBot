import { addSubscriptionPlans } from "./addSubscriptionPlans";

const migrationKeys = {
    subscriptionPlans: 'subscriptionPlans',
}

export const migrationRegistry = {
    [migrationKeys.subscriptionPlans]: addSubscriptionPlans,
}

export const migrationList = Object.values(migrationKeys);
