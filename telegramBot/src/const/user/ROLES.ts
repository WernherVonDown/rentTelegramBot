export enum Roles {
    renter = 'renter',
    rentee = 'rentee',
    admin = 'admin',
    moderator = 'moderator',
}