export interface IInlineOption {
    text: string;
    cb?: string;
    url?: string
} 