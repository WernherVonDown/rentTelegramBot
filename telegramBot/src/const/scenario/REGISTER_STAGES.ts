export const startRegisterStage = 'start';
export const endRegisterStage = 'done';


export enum REGISTER_STAGES {
    city = 'city',
    metro_destance = 'metro_desiance',
    gender = 'gender',
    pets = 'pets',
    smoking = 'smoking',
    nation = 'nation',
    livers = 'livers',
    work = 'work',
    period = 'period',
    photo = 'photo',
    phone_number = 'phone_number',
    price_from = 'price_from',
    price_to = 'price_to',
    address = 'address',
    description = 'description',
    renter_info_reg_part = 'renter_info_reg_part',
    number_of_rooms = 'number_of_rooms',
}